# cyclobs_offline

Offline generation of cyclobs plots

Before starting to use the package, you'll need to install selenium and geckodriver via this command :

conda install selenium geckodriver -c conda-forge

It will allows the program to export plots in png images.