#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

import logging

logger = logging.getLogger(__name__)
import sys

if __name__ == "__main__":
    description = """
        Generate png plots for contatenated SFMR-SAR coloc data.
        """

    pre_parser = argparse.ArgumentParser(add_help=False)
    pre_parser.add_argument(
        "-v", "--version", action="store_true", help="Print version"
    )
    pre_args, remaining_args = pre_parser.parse_known_args()

    from cyclobs_offline._version import __version__

    # Handle the version argument right away
    if pre_args.version:
        print(__version__)
        sys.exit()

    parser = argparse.ArgumentParser(description=description)

    parser.add_argument(
        "-p",
        "--path",
        action="store",
        required=True,
        help="The base path under which save the generated plots",
    )
    parser.add_argument(
        "-a",
        "--api-host",
        action="store",
        required=False,
        default="https://cyclobs.ifremer.fr",
        help="The website for API request (default to https://cyclobs.ifremer.fr)",
    )
    parser.add_argument(
        "--commit",
        action="store",
        required=False,
        default=None,
        help="Processing commit identifier to use in API calls",
    )
    parser.add_argument(
        "--config",
        action="store",
        required=False,
        default=None,
        help="Processing configuration identifier to use in API calls",
    )
    parser.add_argument(
        "--listing",
        action="store",
        required=False,
        default=None,
        help="Processing listing identifier to use in API calls",
    )
    parser.add_argument(
        "--debug",
        action="store_true",
        default=False,
        help="Enable debug logging level.",
    )
    args = parser.parse_args()

    if args.debug:
        logger.setLevel(logging.DEBUG)
        logging.basicConfig(level=logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        logging.basicConfig(level=logging.INFO)

    from cyclobs_offline.sfmr_coloc_plots import create_concat_plots
    from cyclobs_offline.sfmr_plots import sfmr_stats_plots

    sfmr_stats_plots(
        args.path,
        commit=args.commit,
        config=args.config,
        listing=args.listing,
        api_host=args.api_host,
    )
    create_concat_plots(
        args.api_host,
        args.path,
        commit=args.commit,
        config=args.config,
        listing=args.listing,
    )
