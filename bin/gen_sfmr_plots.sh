#!/bin/bash

# Generate stats plots from coloc products

# Usage #
# -dbd for the database used to get sfmr tracks from (required)
# -api for the api host used to make some requests to (required)
# -p for the path under which to save plots (required)
# -r for the path under which to save reports (required)
set -x
env | grep CONDA
which python

GETOPT=$(getopt -n $0 -o d:a:p:f:r:h:: --long help -- "$@")
eval set -- "$GETOPT"

while true; do
    case "$1" in
        -d) database=$2 ; shift 2 ;;
        -a) api_host=$2 ; shift 2 ;;
        -p) save_path=$2 ; shift 2 ;;
        -f) sfmr_file_path=$2 ; shift 2 ;;
        -r) report_path=$2 ; shift 2 ;;
                -- ) shift ; break ;;
        * ) echo "unknown opt $1" >&2 ; exit 1 ;;
    esac
done

script_dir=$(dirname $(readlink -f "$0")) # ie $CONDA_PREFIX/bin

echo $export_path

mkdir -p "$report_path"
mkdir -p "$report_path/logs"

echo "Generating and saving sfmr plots"

python "$script_dir/gen_sfmr_plots.py" -dbd "$database" -api "$api_host" -p "$save_path" -sfmr "$sfmr_file_path" >"$report_path/logs/gen_sfmr_coloc_plots.log" 2>&1

cat "$report_path/logs/gen_sfmr_coloc_plots.log"
