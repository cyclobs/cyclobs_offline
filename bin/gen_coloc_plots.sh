#!/bin/bash

# Generate stats plots from coloc products

# Usage #
# -p for the path under which to save plots (required)
# -r for the path under which to save reports (required)
set -x
env | grep CONDA
which python

GETOPT=$(getopt -n $0 -o p:e:r:h:: --long help -- "$@")
eval set -- "$GETOPT"

while true; do
    case "$1" in
        -p) product_path=$2 ; shift 2 ;;
        -e) export_path=$2 ; shift 2 ;;
        -r) report_path=$2 ; shift 2 ;;
        -- ) shift ; break ;;
        * ) echo "unknown opt $1" >&2 ; exit 1 ;;
    esac
done

script_dir=$(dirname $(readlink -f "$0")) # ie $CONDA_PREFIX/bin

echo $export_path

mkdir -p "$export_path"
mkdir -p "$export_path/plots"
mkdir -p "$export_path/charts"
mkdir -p "$export_path/charts/acquisition-years"
mkdir -p "$export_path/charts/basins"
mkdir -p "$export_path/charts/categories"
mkdir -p "$export_path/maps"
mkdir -p "$report_path"
mkdir -p "$report_path/logs"

echo "Generating all plots from coloc products"

python "$script_dir/gen_coloc_plots.py" -p "$product_path" -e "$export_path">"$report_path/logs/gen_coloc_plots.log" 2>&1

cat "$report_path/logs/gen_coloc_plots.log"
