#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import glob
import os
import copy
import logging
logger = logging.getLogger(__name__)


def add_to_tex(cyclone_name, mission, acq_date, source, source_pret, path, d, tex_str):
    print(os.path.join(path, d, f"*{source}*_img.png"))
    img = os.path.basename(glob.glob(os.path.join(path, d, f"*{source}*_img.png"))[0])
    profile = os.path.basename(glob.glob(os.path.join(path, d, f"*{source}*_profile.png"))[0])
    scat = os.path.basename(glob.glob(os.path.join(path, d, f"*{source}*_scat.png"))[0])

    img = img.replace("_", "\_")
    profile = profile.replace("_", "\_")
    scat = scat.replace("_", "\_")

    tex_str += """
\section{""" + f"{cyclone_name}- {acq_date} - {mission} - {source_pret}" + """}
\\begin{figure}[h]
\centering
\includegraphics[width=0.48\\textwidth]{Figures/SFMR\_SAR/""" + img + """}
\includegraphics[width=0.48\\textwidth]{Figures/SFMR\_SAR/""" + scat + """}
\includegraphics[scale=0.5]{Figures/SFMR_SAR/""" + profile + """}
\caption{Graphics comparing co-located SAR and SFMR data. (Top left) SAR wind speed and SFMR trajectory. 
(Top right) SFMR wind speed vs SAR wind speed. (Middle) SFMR and SAR wind speed profile.}
\end{figure}
\\newpage
    """

    return tex_str


if __name__ == "__main__":
    description = """

        """
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("-p", "--path", action="store", required=True,
                        help="The base path under which save the generated plots")
    parser.add_argument("--debug", action="store_true", default=False,
                        help="Enable debug logging level.")
    args = parser.parse_args()

    if args.debug:
        logger.setLevel(logging.DEBUG)
        logging.basicConfig(level=logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        logging.basicConfig(level=logging.INFO)

    dirs = [name for name in os.listdir(args.path) if os.path.isdir(os.path.join(args.path, name))]

    tex_str = ""
    hrd_not_found_count = 0
    failed_nesdis = 0
    tot_count = 0

    for d in dirs:
        splt = d.split("_")
        cyclone_name = splt[0]
        mission = splt[1]
        acq_date = splt[2].replace("-", "/")
        source = splt[3] + '_' + splt[4]
        source_pret = source.replace("_", " ").upper()

        if source_pret == "SFMR NESDIS":
            try:
                tex_str = add_to_tex(cyclone_name, mission, acq_date, source, source_pret, args.path, d, tex_str)
                tot_count += 1
            except Exception as e:
                failed_nesdis += 1
                logger.error("Failed to generate lines for NESDIS")

            d_cp = copy.copy(d)
            d_cp = d_cp.replace(source, "sfmr_HRD")
            try:
                tex_str = add_to_tex(cyclone_name, mission, acq_date, "sfmr_HRD", "SFMR HRD", args.path, d_cp, tex_str)
                tot_count += 1
            except Exception as e:
                hrd_not_found_count += 1
                logger.error("Failed to generate lines for HRD")

    logger.info(f"Total count : {tot_count}. HRD fail count : {hrd_not_found_count}. NESDIS failed count : {failed_nesdis}")
    with open("result.tex", "w") as f:
        f.write(tex_str)
