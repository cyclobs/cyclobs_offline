#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import glob
import os
import copy
import datetime
import logging
logger = logging.getLogger(__name__)


def add_to_tex(cyclone_name, mission1, mission2, acq_date, path, tex_str, only_SAR=False):
    #print(os.path.join(path, d, f"*{source}*_img.png"))
    print(os.path.join(path, f"*_image0.png"))
    img0 = os.path.basename(glob.glob(os.path.join(path, f"*_image0.png"))[0])
    img1 = os.path.basename(glob.glob(os.path.join(path, f"*_image1.png"))[0])

    img0 = img0.replace("_", "\_")
    img1 = img1.replace("_", "\_")

    analysis0 = os.path.basename(glob.glob(os.path.join(path, f"*analysis0.png"))[0])
    analysis0 = analysis0.replace("_", "\_")

    width = "0.48"
    if only_SAR:
        width = "0.37"
        analysis1 = os.path.basename(glob.glob(os.path.join(path, f"*analysis1.png"))[0])
        analysis2 = os.path.basename(glob.glob(os.path.join(path, f"*analysis2.png"))[0])
        analysis3 = os.path.basename(glob.glob(os.path.join(path, f"*analysis3.png"))[0])
        analysis1 = analysis1.replace("_", "\_")
        analysis2 = analysis2.replace("_", "\_")
        analysis3 = analysis3.replace("_", "\_")

    tex_str += """
\section{""" + f"{cyclone_name}- {acq_date} - {mission1} vs {mission2}" + """}
\\begin{figure}[h]
\centering
\includegraphics[width=""" + width + """\\textwidth]{Figures/SAT\_SAT/""" + img0 + """}
\includegraphics[width=""" + width + """\\textwidth]{Figures/SAT\_SAT/""" + img1 + """}
\includegraphics[width=""" + width + """\\textwidth]{Figures/SAT\_SAT/""" + analysis0 + """}
"""
    if only_SAR:
        tex_str += """\includegraphics[width=""" + width + """\\textwidth]{Figures/SAT\_SAT/""" + analysis1 + """}
\includegraphics[width=""" + width + """\\textwidth]{Figures/SAT\_SAT/""" + analysis2 + """}
\includegraphics[width=""" + width + """\\textwidth]{Figures/SAT\_SAT/""" + analysis3 + """}
"""

    tex_str += """
\caption{Graphics comparing co-located """ + mission1 + """ and """ + mission2 + """ data. (Top left) """+ mission1 +""" colocated portion
with its bounding box. 
(Top right) """+ mission2 +""" colocated portion
with its bounding box. (Other figures) Comparisons between the two acquisitions variables.}
\end{figure}
\\newpage
"""

    return tex_str


if __name__ == "__main__":
    description = """

        """
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("-p", "--path", action="store", required=True,
                        help="The base path under which save the generated plots")
    parser.add_argument("--debug", action="store_true", default=False,
                        help="Enable debug logging level.")
    args = parser.parse_args()

    if args.debug:
        logger.setLevel(logging.DEBUG)
        logging.basicConfig(level=logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        logging.basicConfig(level=logging.INFO)

    #dirs = [name for name in os.listdir(args.path) if os.path.isdir(os.path.join(args.path, name))]
    coloc_couples = [("S1A", "RS2"), ("S1B", "RS2"), ("C-Band_SAR", "SMAP")]


    tex_str = ""
    hrd_not_found_count = 0
    failed = 0
    tot_count = 0

    idx = 0
    for d in coloc_couples:
        dir = f"{d[0]}_{d[1]}"
        dirs = [name for name in os.listdir(os.path.join(args.path, dir)) if os.path.isdir(os.path.join(args.path, dir, name))]
        dirs.sort(key = lambda x: datetime.datetime.strptime(x.split("_")[-1], "%d-%m-%Y"))

        for file_dir in dirs:
            splt = file_dir.split("_")
            cyclone_name = splt[-2]
            acq_date = splt[-1].replace("-", "/")
            full_path = os.path.join(args.path, dir, file_dir)
            only_SAR = idx == 0 or idx == 1
            try:
                tex_str = add_to_tex(cyclone_name, d[0], d[1], acq_date, full_path, tex_str, only_SAR)
                tot_count += 1
            except Exception as e:
                failed += 1
                logger.error("Failed to generate lines" + str(e))
        idx += 1

    logger.info(f"Total count : {tot_count}. Fail count : {failed}.")
    with open("result_coloc.tex", "w") as f:
        f.write(tex_str)
