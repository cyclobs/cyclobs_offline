#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

import logging
logger = logging.getLogger(__name__)

if __name__ == "__main__":
    description = """

        """
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("-api", "--api-host", action="store", required=True,
                        help="The api host used to do some requests in the script")
    parser.add_argument("-sfmr", "--sfmr_file_path", action="store", required=True,
                        help="The path under which sfmr coloc file path are located")
    parser.add_argument("-p", "--save_path", action="store", required=True,
                        help="The path under which save the generated plots")
    parser.add_argument("--debug", action="store_true", default=False,
                        help="Enable debug logging level.")
    args = parser.parse_args()

    if args.debug:
        logger.setLevel(logging.DEBUG)
        logging.basicConfig(level=logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        logging.basicConfig(level=logging.INFO)

    from cyclobs_offline.sfmr_coloc_plots import create_plots

    create_plots(args.api_host, args.sfmr_file_path, save_path=args.save_path)
