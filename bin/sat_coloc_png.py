#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

import logging
logger = logging.getLogger(__name__)

if __name__ == "__main__":
    description = """

        """
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("-p", "--path", action="store", required=True,
                        help="The base path under which save the generated plots")
    parser.add_argument("--debug", action="store_true", default=False,
                        help="Enable debug logging level.")
    args = parser.parse_args()

    if args.debug:
        logger.setLevel(logging.DEBUG)
        logging.basicConfig(level=logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        logging.basicConfig(level=logging.INFO)

    from cyclobs_offline.sat_coloc import all_sat_coloc_png

    all_sat_coloc_png(args.path)

