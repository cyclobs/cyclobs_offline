#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import argparse
import logging
import os
from cyclobs_utils.cyclobs_config import (
    SMAP_SHORT,
    SMOS_SHORT,
    RS2_SHORT,
    RCM1_SHORT,
    RCM2_SHORT,
    RCM3_SHORT,
    LBAND_INSTRUMENT_SHORT,
)
from cyclobs_utils import spd2cat
from urllib.parse import quote


logging.basicConfig(
    level=logging.INFO, format="%(asctime)s [%(levelname)s] %(message)s"
)
logger = logging.getLogger(__name__)
if __name__ == "__main__":

    pre_parser = argparse.ArgumentParser(add_help=False)
    pre_parser.add_argument(
        "-v", "--version", action="store_true", help="Print version"
    )
    pre_args, remaining_args = pre_parser.parse_known_args()

    from cyclobs_offline._version import __version__

    # Handle the version argument right away
    if pre_args.version:
        print(__version__)
        sys.exit()

    parser = argparse.ArgumentParser(
        description="A script that generates coloc stats plots from coloc files. Coloc files must be given either through piping, -p or -l."
    )
    parser.add_argument(
        "-p", "--files_path", help="Path where the coloc files are.", required=False
    )
    parser.add_argument(
        "-l",
        "--listing",
        action="append",
        help="Path to listing of coloc files. This argument can be provided multiple times to create a list of files.",
    )

    parser.add_argument(
        "-e", "--export_path", help="Path where the plots will be created"
    )
    parser.add_argument(
        "-a",
        "--api-host",
        help="The website for API request. Needed for basin/category plots.",
        default="https://cyclobs.ifremer.fr",
    )
    parser.add_argument(
        "--commit",
        action="store",
        required=False,
        default=None,
        help="Processing commit identifier to use in API calls",
    )
    parser.add_argument(
        "--config",
        action="store",
        required=False,
        default=None,
        help="Processing configuration identifier to use in API calls",
    )
    parser.add_argument(
        "--listing-name",
        action="store",
        required=False,
        default=None,
        help="Processing listing identifier to use in API calls",
    )
    parser.add_argument(
        "-m",
        "--mission",
        default=None,
        help="Filter by mission or instrument the plots to generate.",
    )
    parser.add_argument(
        "--comp", action="store_true", default=False, help="Generate comparison plots"
    )
    parser.add_argument(
        "--map", action="store_true", default=False, help="Generate map plots"
    )
    parser.add_argument(
        "--basin-cat",
        action="store_true",
        default=False,
        help="Generate basin and category plots",
    )
    parser.add_argument(
        "--years-bar",
        action="store_true",
        default=False,
        help="Generate acquisition year plots",
    )

    parser.add_argument(
        "--debug",
        action="store_true",
        default=False,
        help="Enable debug logging level.",
    )

    args = parser.parse_args()
    if args.debug:
        logger.setLevel(logging.DEBUG)
        logging.basicConfig(level=logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        logging.basicConfig(level=logging.INFO)

    logger.info(f"LISTING: {args.listing}")

    if args.files_path:
        # Make sure the provided path exists
        if not os.path.exists(args.files_path):
            logger.error(
                "The provided files_path does not exist: {}".format(args.files_path)
            )
            sys.exit(1)

        # Construct the pattern to search for .nc files
        search_pattern = os.path.join(args.files_path, "*.nc")

        # Use glob to find all files with .nc extension
        files = glob.glob(search_pattern)

        if not files:
            logger.warning(
                "No .nc files found in the specified directory: {}".format(
                    args.files_path
                )
            )
        else:
            logger.info("Found {} .nc files in {}".format(len(files), args.files_path))
            # You can now process these files as per your requirement
            # for file in nc_files:
            #     process_file(file)  # Replace with your file processing function
    elif args.listing:
        listings = args.listing if isinstance(args.listing, list) else [args.listing]
        files = []
        for listing in listings:
            # Make sure the provided path exists
            if not os.path.exists(listing):
                logger.error(
                    "The provided listing file does not exist: {}".format(listing)
                )
                sys.exit(1)

            # Read the file and get the list of files
            with open(listing, "r") as f:
                files.extend(f.readlines())

        # Remove any leading/trailing whitespaces
        files = [f.strip() for f in files]

        # Filter out any empty lines
        files = [f for f in files if f]

        if not files:
            logger.warning(
                "No files found in the specified listing files: {}".format(listings)
            )
            sys.exit(1)
        else:
            logger.info(
                "Found {} files in the listing files: {}".format(len(files), listings)
            )
    # Check if data is being piped in
    elif not sys.stdin.isatty():
        files = []
        files_pre_check = sys.stdin.read().strip().split("\n")
        # Iterate through the list of files and check if each file exists
        for file in files_pre_check:
            if not os.path.isfile(file):
                logger.warning(f"File does not exist: {file}")
            else:
                files.append(file)
    else:
        parser.error(
            "No input source provided. Please specify a files path or pipe in data."
        )

    export_path = args.export_path
    mission_filter = args.mission

    # If no specific type of plot is given in args, generate all of them
    all_plots = False
    if not args.comp and not args.map and not args.basin_cat and not args.years_bar:
        all_plots = True
        logger.info("All types of plot will be generated.")

    if export_path is None:
        logger.info("-e argument not set, plots will be saved in this repertory")
        export_path = os.getcwd() + "/"

    # Main Process
    from cyclobs_offline.gen_coloc_comp_plots import *
    from cyclobs_offline.gen_coloc_stats_plots import *
    from cyclobs_offline.gen_coloc_map import *

    missions = [
        SMAP_SHORT,
        SMOS_SHORT,
        RS2_SHORT,
        RCM1_SHORT,
        RCM2_SHORT,
        RCM3_SHORT,
        LBAND_INSTRUMENT_SHORT,
    ]

    if mission_filter is not None:
        missions = [m for m in missions if m in mission_filter]

    logger.info(f"Total file count: {len(files)}")

    logger.info(
        f"Plots will be generated for the following missions or instruments : {missions}"
    )

    for mission in missions:
        # Get coloc datasets
        logger.info("Get " + mission + " colocs")
        mission_files = get_one_mission_coloc_datasets_generic(
            files, mission_or_instr=mission
        )

        # Concatenate datasets
        logger.info("Concatenate " + mission + " files")
        mission_dict = concatenate_datasets_new(mission_files)

        # Get plots colormaps
        logger.info("Get " + mission + " colormap")
        colormap_mission = get_colormap(mission_dict.keys(), colors="different")

        if all_plots or args.comp:
            # Generate coloc wind speed comparison plots
            logger.info("Generating " + mission + " plots")
            build_comp_plots(mission_dict, colormap_mission, export_path)

            # Generate coloc stats plots
            logger.info("Generating " + mission + " stats plots")
            build_comp_stats_plots(mission_dict, colormap_mission, export_path)

        # Coloc basins and categories pie charts generation
        if all_plots or args.basin_cat:
            url_req = f"{args.api_host}/app/api/getData?include_cols=data_url,vmax,basin&instrument=C-Band_SAR"
            if args.commit and args.config and args.listing_name:
                url_req += f"&commit={quote(args.commit)}&config={quote(args.config)}&listing={quote(args.listing_name)}"
            logger.info(f"Getting data from API with {url_req}")
            df_api = pd.read_csv(url_req, keep_default_na=False)
            df_api["file_base"] = df_api["data_url"].apply(
                lambda x: "-".join(
                    os.path.basename(x).split("-")[0:6]
                )  # keep rs2--owi-cm-20130701t104119-20130701t104235
            )
            # print("Len 1", len(df_api.index))
            # df_api = df_api.drop_duplicates(subset=["coloc_file"])
            df_api["curr_cyc_categ"] = df_api["vmax (m/s)"].apply(
                lambda x: spd2cat(x, unit="m/s")
            )
            logger.info(
                "Generating " + mission + " basins charts and categories charts"
            )
            for key in mission_files:
                # Keep API lines for files in mission_files[key]
                # df_api["file_base"] = ["s1a-owi-cm...-42857.nc", ]
                # mission_files[key] = ["/full/path/sat_coloc_SMOSFILE__SARFILE.nc", ]
                df_api_mission = df_api.loc[
                    df_api["file_base"].apply(
                        lambda x: any(
                            x.replace(".nc", "") in mission_file
                            for mission_file in mission_files[key]
                        )
                    )
                ]
                if len(df_api_mission.index) < len(mission_files[key]):
                    missing_files = [
                        f
                        for f in mission_files[key]
                        if not any(
                            api_file in os.path.basename(f).replace(".nc", "")
                            for api_file in df_api_mission["file_base"]
                        )
                    ]
                    if len(missing_files) > 0:
                        # logger.warning(f"Missing files in API data: {missing_files}")
                        msg = (
                            f"For key {key} API data and coloc files counts do not match: count from listing: "
                            f"{len(mission_files[key])}, count from API: {len(df_api_mission.index)}. Missing files in API: {len(missing_files)}"
                        )
                        logger.error(msg)
                        logger.debug(f"Missing files in API data: {missing_files}")
                        raise ValueError(msg)

                title = (
                    key.replace("_", "/").upper()
                    + " ("
                    + str(len(mission_files[key]))
                    + " co-locations)"
                )
                create_basin_charts(
                    df_api_mission,
                    export_path,
                    "Basins " + title,
                    filename="basins-" + key,
                )
                create_categories_charts(
                    df_api_mission,
                    export_path,
                    "Categories " + title,
                    filename="cat-" + key,
                )

        other_missions = cyclobs_config.COLOC_COMBINATION_SHORT[mission]
        if all_plots or args.map:
            # Coloc maps generation
            logger.info("Generating " + mission + " map")
            for other_m in other_missions:
                # If other_m is an instrument, take all mission for that instrument so that all mission for that
                # instrument shows on the map.
                if other_m in cyclobs_config.INSTRUMENTS_SHORT:
                    other_missions_for_instr = (
                        cyclobs_config.INSTRUMENTS_MISSIONS_SHORT[other_m]
                    )
                    logger.debug(f"Mission_files: {mission_files}")
                    curr_combination_files = {
                        f"{mission}_{o}": mission_files[f"{mission}_{o}"]
                        for o in other_missions_for_instr
                        if f"{mission}_{o}" in mission_files
                    }
                else:
                    comb = f"{mission}_{other_m}"
                    if comb not in mission_files:
                        logger.warning(f"Skipping combination {comb}")
                        continue
                    curr_combination_files = {comb: mission_files[comb]}
                if curr_combination_files == {}:
                    logger.warning(f"No data to create map for {mission} and {other_m}")
                    continue
                logger.info(f"Generating map for {mission} and {other_m}")
                acqMap(
                    mission,
                    curr_combination_files,
                    export_path,
                    f"{mission}-{other_m}",
                    title=f"{mission} and {other_m} co-locations footprints.",
                )

        if all_plots or args.years_bar:
            # Coloc years bar charts generation
            logger.info("Generating " + mission + " years bar chart")
            for other_m in other_missions:
                # If other_m is an instrument, take all mission for that instrument so that all mission for that
                # instrument shows on the map.
                if other_m in cyclobs_config.INSTRUMENTS_SHORT:
                    other_missions_for_instr = (
                        cyclobs_config.INSTRUMENTS_MISSIONS_SHORT[other_m]
                    )
                    curr_combination_files = {
                        f"{mission}_{o}": mission_files[f"{mission}_{o}"]
                        for o in other_missions_for_instr
                        if f"{mission}_{o}" in mission_files
                    }
                else:
                    comb = f"{mission}_{other_m}"
                    if comb not in mission_files:
                        logger.warning(f"Skipping combination {comb}")
                        continue
                    curr_combination_files = {comb: mission_files[comb]}
                yearsBarChart(
                    mission,
                    curr_combination_files,
                    export_path,
                    f"{mission}-{other_m}",
                    title=f"{mission}/{other_m} co-localisations years.",
                )

        logger.info(mission + " plots generated")
