from setuptools import setup
import glob

setup(name='cyclobs_offline',
      description='Production of co-localization stats plots',
      url='https://gitlab.ifremer.fr/cyclobs/cyclobs_offline',
      author="Bastien Anfray",
      author_email="bastien.anfray@ifremer.fr",
      license='GPL',
      setup_requires=['setuptools_scm'],
      use_scm_version={'write_to': '%s/_version.py' % "cyclobs_offline"},
      packages=['cyclobs_offline'],
      zip_safe=False,
      scripts=glob.glob('bin/**'),
      install_requires=[
          "xarray",
          "numpy",
          "pandas",
          "dask",
          'holoviews',
          'geoviews',
          'panel',
          "bokeh",
          "pyproj",
          "datashader",
          "netcdf4",
          "geopandas",
          "humanize",
          'matplotlib',
          "shapely",
          'pathurl>=0.0.dev0',
          'cyclobs_utils>=0.0.dev0',
          'colormap_ext>=0.0.dev0',
          "cyclone_formatter>=0.0.dev0",
          "geo_shapely>0.0.dev0"
      ]
      )
