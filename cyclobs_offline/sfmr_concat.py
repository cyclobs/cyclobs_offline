import logging
import io
import requests

logger = logging.getLogger(__name__)


from cyclobs_utils import (
    get_all_missions,
    get_all_instruments,
    get_missions_per_instrument,
    filter_df,
)
from .utils import get_track_sources
import os
import pathurl
import xarray as xr
import datetime
import pandas as pd
import numpy as np
import copy


delta_times = [7200, 1800]
dtime_colors = {7200: "red", 1800: "cyan"}
dtime_markers = {7200: "^", 1800: "o"}


def open_coloc_files(api_host, commit=None, config=None, listing=None):
    logger.info("Generating SFMR concatenated coloc data...")
    sfmr_sources = get_track_sources(
        api_host, commit=commit, config=config, listing=listing
    )
    sfmr_sources = [s for s in sfmr_sources if "sfmr" in s]

    data_sources = {}
    for source in sfmr_sources:
        params = {
            "track_source": source,
            "product_type": "gridded",
            "include_cols": "all",
        }
        if commit is not None:
            params["commit"] = commit
        if config is not None:
            params["config"] = config
        if listing is not None:
            params["listing"] = listing

        api_url = f"{api_host}/app/api/getData"
        response = requests.get(api_url, params=params)
        if response.status_code == 200:
            df_coloc = pd.read_csv(io.StringIO(response.text))
        else:
            error_msg = f"Failed to get data from API: {response.status_code}"
            logger.error(error_msg)
            raise RuntimeError(error_msg)

        # Only keep coloc with a generated product file
        df_coloc = df_coloc[
            (df_coloc["coloc_file"] != "") & (~pd.isna(df_coloc["coloc_file"]))
        ]
        logger.info("Opening product coloc netCDF files...")
        list_cols = [
            "sfmr_wind_speed",
            "sat_wind_speed",
            "sat_nrcs_cross",
            "sat_nrcs_co",
            "sat_incidence_angle",
        ]
        list_col_opt = ["sat_nrcs_cross", "sat_nrcs_co", "sat_incidence_angle"]

        df_coloc["coloc_file"] = df_coloc["coloc_file"].apply(
            lambda x: pathurl.toPath(x, schema="dmz")
        )

        coloc_ds = df_coloc["coloc_file"].apply(lambda x: xr.open_dataset(x)).to_list()

        for i in range(len(coloc_ds)):
            ds = coloc_ds[i]
            curr_list = copy.copy(list_cols)
            if not all(var in ds.data_vars for var in list_cols):
                logger.warning(
                    f"Not all required variables found in coloc file: {df_coloc['coloc_file'].iloc[i]}."
                )
                coloc_ds[i] = None
                continue
            coloc_ds[i] = [ds[curr_list]]

        df_coloc["ds"] = coloc_ds

        data_sources[source] = df_coloc

    return data_sources


def concat(df_coloc):
    ds_list = [d[0] for d in df_coloc["ds"].tolist() if d is not None]
    return xr.concat(
        ds_list, "delta_time", data_vars="minimal", coords="minimal", compat="override"
    )


def concat_data(api_host, commit=None, config=None, listing=None):
    instruments = get_all_instruments(
        api_host, commit=commit, config=config, listing=listing
    )
    missions = get_missions_per_instrument(
        api_host, commit=commit, config=config, listing=listing
    )

    data_sources = open_coloc_files(
        api_host, commit=commit, config=config, listing=listing
    )
    instr_ds = {}
    mis_ds = {}

    logger.info("Starting SFMR coloc concatenation...")
    print(instruments)
    print(missions)

    for sfmr in data_sources:
        coloc_df = data_sources[sfmr]
        instr_ds[sfmr] = {}
        mis_ds[sfmr] = {}

        for inst in instruments:
            df_instr = filter_df(coloc_df, list_instruments=[inst])
            if len(df_instr.index) > 0:
                ds_instr = concat(df_instr)
                ds_instr = ds_instr.sortby("delta_time")
                instr_ds[sfmr][inst] = {}
                for dt in delta_times:
                    ds_instr_sub = ds_instr.sel(
                        delta_time=slice(f"-{dt} seconds", f"{dt} seconds")
                    )
                    df = ds_instr_sub.to_dataframe()
                    df = df.loc[
                        (df["sat_nrcs_cross"] > 0.00001) & (df["sat_nrcs_co"] > 0.00001)
                    ]
                    instr_ds[sfmr][inst][dt] = df

                for mis in missions[inst]:
                    df_mis = filter_df(coloc_df, list_missions=[mis])
                    if len(df_mis.index) > 0:
                        ds_mis = concat(df_mis)
                        ds_mis = ds_mis.sortby("delta_time")
                        mis_ds[sfmr][mis] = {}
                        for dt in delta_times:
                            ds_mis_sub = ds_mis.sel(
                                delta_time=slice(f"-{dt} seconds", f"{dt} seconds")
                            )
                            df = ds_mis_sub.to_dataframe()
                            df = df.loc[
                                (df["sat_nrcs_cross"] > 0.00001)
                                & (df["sat_nrcs_co"] > 0.00001)
                            ]
                            mis_ds[sfmr][mis][dt] = df

        logger.info("Finished generating sfmr coloc concat.")

    return instr_ds, mis_ds
