import logging

logger = logging.getLogger(__name__)

import collections
import os
from cyclobs_offline.gen_coloc_stats_plots import (
    holoTimeBarChart,
    createPieChart,
    create_basin_charts,
    create_categories_charts,
)
import pandas as pd
from cyclobs_utils.cyclobs_config import (
    INSTRUMENTS,
    MISSION_SHORT_TO_LONG,
    MISSIONS,
    INSTRUMENTS_MISSIONS,
    INSTRUMENTS_LONG_TO_SHORT,
    MISSION_LONG_TO_SHORT,
)
from cyclobs_utils import filter_df, spd2cat, maxspd
from bokeh.io import curdoc, export_png
from bokeh.layouts import layout
from bokeh.models import CustomJSTickFormatter
from bokeh.models import Div
from bokeh.palettes import Category10
from bokeh.transform import factor_cmap

import cartopy.crs as ccrs

import geoviews as gv
import holoviews as hv
import geoviews.feature as gf
from urllib.parse import quote

gv.extension("bokeh")
renderer = gv.renderer("bokeh").instance()
import pandas as pd
import geopandas as gpd

#  Create a color list for all classification of tropical cyclone
stormsColor = {
    "storm": "gray",
    "cat-1": "blue",
    "cat-5": "red",
    "cat-4": "orange",
    "cat-3": "yellow",
    "cat-2": "green",
    "dep": "cyan",
}


def create_acq_map(title, gdf, legend, force_points=False, opts={}):
    # Useful variables
    if opts is None:
        opts = {}
    lonOffset = -160
    targetProjection = ccrs.PlateCarree(central_longitude=lonOffset)

    if len(gdf.index) == 0:
        plot = (
            gf.land
            * gf.ocean
            * gf.coastline.opts(
                projection=targetProjection,
                width=1200,
                height=900,
                padding=0.1,
                axiswise=True,
                title=title,
                tools=["hover"],
                global_extent=True,
            )
        )
        hvplot = renderer.get_plot(plot, curdoc()).state
        return hvplot  # layout(hvplot.state, sizing_mode="scale_both")

    # Set Coordinate Reference Systems
    # default crs from input, it's the full def of 4326
    crs_in = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"
    # converting tracks and crs to proj.proj4_init give this error for pyproj<=2.0.0
    # so we change the prime meridian (+pm), to match lonOffset in PlateCarre
    crs_out = "%s +pm=%d" % (crs_in, lonOffset)
    gdf.crs = crs_in
    gdf = gdf.to_crs(crs=crs_out)

    # Create a list of polygon footprints to show all Acquisitions on Map
    # footprints = [gv.Polygons(gdf.loc[gdf[legend] == l], vdims=["color", legend], crs=targetProjection, label=l)
    #              for l in gdf[legend].unique()]
    if len(gdf.index) > 900 or force_points:
        gdf["geometry"] = gdf.centroid
        footprints = [
            gv.Points(
                gdf.loc[gdf[legend] == l],
                vdims=["color", legend],
                crs=targetProjection,
                label=l,
            )
            for l in gdf[legend].unique()
        ]
        # Transform this list of polygon footprints in "holoview plot overlay objects"
        list_footprint_plots = [
            ft.opts(
                color="color",
                tools=["hover", "wheel_zoom"],
                legend_position="top_right",
                show_legend=True,
                size=5,
                alpha=0.7,
            )
            for ft in footprints
        ]
    else:
        footprints = [
            gv.Polygons(
                gdf.loc[gdf[legend] == l],
                vdims=["color", legend],
                crs=targetProjection,
                label=l,
            )
            for l in gdf[legend].unique()
        ]
        # Transform this list of polygon footprints in "holoview plot overlay objects"
        list_footprint_plots = [
            ft.opts(
                fill_color="color",
                tools=["hover", "wheel_zoom"],
                legend_position="top_right",
                show_legend=True,
                fill_alpha=0.6,
            )
            for ft in footprints
        ]

    # list_footprint_plots = [ft.opts(tools=["hover", "wheel_zoom"]) for ft in footprints]

    #  Compute all the "holoview plot overlay objects" to multiply them with the other context overlay to be displayed
    len_list = len(list_footprint_plots)
    pre_plot_polygons = list_footprint_plots[0]
    if len_list > 1:
        for i in range(len_list - 1):
            pre_plot_polygons = pre_plot_polygons * list_footprint_plots[i + 1]

    # Create final plot to draw the Map
    plot = (
        gf.land
        * gf.ocean
        * gf.coastline.opts(
            projection=targetProjection,
            width=1200,
            height=900,
            padding=0.1,
            axiswise=True,
            title=title,
            tools=["hover"],
        )
        * pre_plot_polygons
    ).opts(**opts)

    # plot = plot.redim.range(Latitude=(-70, 70))
    # Code to set axis right no matter what is the longitude offset
    lonAxisTransformCode = """
    if (tick <= -20) {
        return -(360+lonOffset) - tick;
    } else {
        return -lonOffset - tick;
    }
    """

    # This function applies the longitude axis ticks label formatter to the plot
    def update_axis(plot, element):
        b = plot.state
        b.axis[0].formatter = CustomJSTickFormatter(
            args={"lonOffset": lonOffset}, code=lonAxisTransformCode
        )

    # Adding the update tick function to the main plot.
    plot = plot.options(hooks=[update_axis])
    hvplot = renderer.get_plot(plot, curdoc()).state

    return hvplot


def sfmr_map(sfmr_data, mission_color, title="Coloc locations"):
    sfmr_data["geometry"] = gpd.GeoSeries.from_wkt(sfmr_data["track_point"])

    gdf = gpd.GeoDataFrame(sfmr_data, geometry="geometry")
    gdf["color"] = gdf["mission_short"].apply(lambda x: mission_color[x])

    return create_acq_map(
        title,
        gdf,
        legend="mission_short",
        force_points=True,
        opts={
            "xlim": (-25, 130),
            "ylim": (5, 45),
            "width": 700,
            "height": 400,
            "legend_position": "top_left",
        },
    )


def sfmr_years_bar_chart(
    sfmr_data,
    legend_colors,
    filename,
    title="Number of Sat-SFMR colocation per year",
):
    # { "years": ["2018", "2019"],
    #   "RADARSAT-2": [10, 17]
    #   "SENTINEL-1 A" : [5, 9] }
    sfmr_data["acquisition_start_time"] = pd.to_datetime(
        sfmr_data["acquisition_start_time"]
    )
    sfmr_data["years"] = sfmr_data["acquisition_start_time"].apply(lambda x: x.year)

    counts = sfmr_data.groupby(["years", "mission_short"])[["data_url"]].count()

    counts = counts.rename(columns={"data_url": "count"})

    return holoTimeBarChart(
        title,
        df=counts,
        kdims=["years", "mission_short"],
        filename=filename,
    )


def sensorsPieChart(
    sfmr_df,
    mission_color,
    title="Sensors",
    list_instruments=None,
    year=None,
    basin=None,
):
    sfmr_df = filter_df(sfmr_df, list_instruments, year=year, basin=basin)

    counts = sfmr_df.groupby(["mission_short"]).count()

    if len(sfmr_df.index) == 0:
        return Div(text="Cannot build basin pie chart")

    sens_count = {}
    for sens, row in counts.iterrows():
        sens_count[sens] = row["data_url"]

    color = factor_cmap(
        "mission_short",
        palette=list(mission_color.values()),
        factors=list(mission_color.keys()),
    )

    return createPieChart(title, sens_count, "mission_short", color=color)


def catPieChart(
    sfmr_data,
    atcf_df,
    title="Categories",
    list_instruments=None,
    list_missions=None,
    basin=None,
    year=None,
):

    sfmr_data = filter_df(sfmr_data, list_instruments, list_missions, basin, year)
    sfmr_data = sfmr_data.merge(atcf_df, on="data_url", how="left")

    sfmr_data["cat"] = sfmr_data["vmax (m/s)_y"].apply(lambda x: spd2cat(x, unit="m/s"))

    counts = sfmr_data.groupby(["cat"]).count()

    if len(sfmr_data.index) == 0:
        return Div(text="Cannot build category pie chart")

    catsCount = collections.OrderedDict().fromkeys(maxspd.keys(), 0)
    for cat, row in counts.iterrows():
        catsCount[cat] = row["data_url"]

    color = factor_cmap(
        "category", palette=list(stormsColor.values()), factors=list(stormsColor.keys())
    )

    return createPieChart(title, catsCount, "category", color=color)


def sfmr_stats_plots(path_out, commit, config, listing, api_host):
    sfmr_sources = ["sfmr_NESDIS", "sfmr_HRD"]
    atcf_req = (
        f"{api_host}/app/api/getData?include_cols=all&track_source=atcf&"
        f"commit={quote(commit)}&config={quote(config)}&listing={quote(listing)}&instrument=C-Band_SAR"
    )

    logger.info(f"Requesting data from {atcf_req}")

    # atcf_req = "http://localhost" + url_for("api.acquisitionList") + f"?include_cols=all&track_source=atcf"
    df_atcf = pd.read_csv(atcf_req, keep_default_na=False)
    pd.set_option("display.max_colwidth", 1000)

    data_source = {}
    for source in sfmr_sources:
        api_req = (
            f"{api_host}/app/api/getData?include_cols=all&track_source={quote(source)}&"
            f"commit={quote(commit)}&config={quote(config)}&listing={quote(listing)}&instrument=C-Band_SAR"
        )

        logger.info(f"Requesting data from {api_req}")
        # api_req = "http://localhost" + \
        #          url_for("api.acquisitionList") + f"?track_source={source}&" \
        #                                           f"product_type={product_type}&include_cols=all"
        df_coloc = pd.read_csv(api_req, keep_default_na=False)
        # Only keep coloc with a generated product file
        df_coloc = df_coloc[
            (df_coloc["coloc_file"] != "") & (~pd.isna(df_coloc["coloc_file"]))
        ]

        # API can return duplicates, this is a normal behavior. (For example, several basin for same colocation)
        # As a result search and remove duplicates
        df_coloc.drop_duplicates(subset=["data_url", "track_file"])
        df_coloc = df_coloc.loc[df_coloc["data_url"].isin(df_atcf["data_url"])]
        df_coloc["curr_cyc_categ"] = df_coloc["vmax (m/s)"].apply(
            lambda x: spd2cat(x, unit="m/s")
        )
        data_source[source] = df_coloc

    instruments = INSTRUMENTS
    missions_short = MISSION_SHORT_TO_LONG.keys()
    missions = INSTRUMENTS_MISSIONS

    allMissionsColor = {}
    for i, mission in enumerate(missions_short):
        allMissionsColor[mission] = Category10[10][i]

    for sfmr in sfmr_sources:
        logger.info(f"Creating plots for {sfmr}")
        logger.info(f"Coloc count : {len(data_source[sfmr].index)}")
        logger.info("Creating map")
        sfmr_label = sfmr.replace("_", " ").upper()
        map_title = (
            f"{sfmr_label} co-locations (count : {len(data_source[sfmr].index)})"
        )
        map = sfmr_map(data_source[sfmr], allMissionsColor, title=map_title)
        # hv.save(map, os.path.join(path_out, f"{sfmr}_map.png"))
        export_png(map, filename=os.path.join(path_out, f"{sfmr}_map.png"))

        logger.info("Creating years bar chart")
        years_bar_title = f"Sat - {sfmr_label} co-locations year count (count : {len(data_source[sfmr].index)})"
        sfmr_years_bar_chart(
            data_source[sfmr],
            allMissionsColor,
            os.path.join(path_out, f"{sfmr}_years_bar_chart.png"),
            title=years_bar_title,
        )
        logger.info("Creating sensors pie chart")
        sensor_title = f"Sensors co-located with {sfmr_label} (count : {len(data_source[sfmr].index)})"
        sensor_plots = sensorsPieChart(
            data_source[sfmr], allMissionsColor, title=sensor_title
        )
        export_png(
            sensor_plots,
            filename=os.path.join(path_out, f"{sfmr}_sensors_pie_chart.png"),
        )
        # hv.save(sensor_plots, os.path.join(path_out, f"{sfmr}_sensors_pie_chart.png"))
        for inst in instruments:
            inst_short = INSTRUMENTS_LONG_TO_SHORT[inst]
            df_instr = filter_df(data_source[sfmr], list_instruments=[inst])
            logger.info(f"Creating plots for {sfmr} {inst}")
            logger.info(f"Coloc count : {len(df_instr.index)}")
            if len(df_instr.index) == 0:
                continue
            logger.info("Creating categories pie chart")
            df_cat_instr = df_instr.merge(df_atcf, on="data_url", how="left")
            df_cat_instr["curr_cyc_categ"] = df_cat_instr["vmax (m/s)_y"].apply(
                lambda x: spd2cat(x, unit="m/s")
            )
            create_categories_charts(
                df_cat_instr,
                path_out,
                f"Storm categories for {sfmr_label} / {inst_short} co-locations.\n(Count: {len(df_cat_instr.index)})",
                f"{sfmr}_{inst_short}_categories_pie_chart",
            )
            logger.info("Creating basin pie chart")
            create_basin_charts(
                df_instr,
                path_out,
                f"Basins for {sfmr_label} / {inst_short} co-locations\n(Count: {len(df_instr.index)})",
                f"{sfmr}_{inst_short}_basin_pie_chart",
            )

            mss = missions[inst]
            for m in mss:
                m_short = MISSION_LONG_TO_SHORT[m]
                logger.info(f"Creating plots for {sfmr} {inst} {m}")
                df_mission = filter_df(
                    data_source[sfmr], list_instruments=[inst], list_missions=[m]
                )
                logger.info(f"Coloc count : {len(df_mission.index)}")
                logger.info("Creating categories pie chart")
                df_cat_mis = df_mission.merge(df_atcf, on="data_url", how="left")
                df_cat_mis["curr_cyc_categ"] = df_cat_mis["vmax (m/s)_y"].apply(
                    lambda x: spd2cat(x, unit="m/s")
                )
                create_categories_charts(
                    df_cat_mis,
                    path_out,
                    f"Storm categories for {sfmr_label} / {m_short} co-locations\n(Count: {len(df_cat_mis.index)})",
                    f"{sfmr}_{inst_short}_{m_short}_categories_pie_chart",
                )
                logger.info("Creating basin pie chart")
                create_basin_charts(
                    df_mission,
                    path_out,
                    f"Basins for {sfmr_label} / {m_short} co-locations\n(Count: {len(df_mission.index)})",
                    f"{sfmr}_{inst_short}_{m_short}_basin_pie_chart",
                )
