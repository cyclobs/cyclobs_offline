import logging

logger = logging.getLogger(__name__)

import os
import xarray as xr
import panel as pn
import numpy as np
import glob
import cyclobs_utils.cyclobs_config as cyclobs_config
import pandas as pd
import holoviews as hv
from holoviews.operation.datashader import rasterize
from holoviews import opts

hv.extension("bokeh")
# from bokeh.io import export_png, save
# from bokeh.io.export import get_screenshot_as_png

from cyclobs_offline.do_stat import do_stat

import warnings
from dask.array.core import PerformanceWarning

warnings.filterwarnings("ignore", category=PerformanceWarning)

missions_short = cyclobs_config.MISSION_LONG_TO_SHORT


def get_one_mission_coloc_datasets_generic(files, mission_or_instr=""):
    # Get all combination of possible coloc
    coloc_mapping = cyclobs_config.COLOC_COMBINATION_SHORT

    files_dict = {}
    if mission_or_instr in coloc_mapping:
        # Get available comparisons mission_or_instr and instrument for the current mission_or_instr
        other_missions_or_instr = coloc_mapping[mission_or_instr]

        if mission_or_instr in cyclobs_config.INSTRUMENTS_SHORT:
            missions_ok = cyclobs_config.INSTRUMENTS_MISSIONS_SHORT[mission_or_instr]
        else:
            missions_ok = [mission_or_instr]

        # Have a set of combinations to remove duplicates
        combination_set = set()
        for file in files:
            # sat_coloc_s1a-ew-owi-cm-20241106t234334-20241106t234437-000003-06EAB8_ll_gd__rcm2-sclnc-owi-cm-20241106t234158-20241106t234314-00003-______ll_gd.nc
            filename = os.path.basename(file)
            split_coloc = (
                filename.replace("sat_coloc_", "")
                .replace(".nc", "")
                .replace("______ll_gd", "temp_placeholder_for_split")
                .split("__")
            )  # gives : [s1a-ew-owi-cm-20241106t234334-20241106t234437-000003-06EAB8_ll_gd, rcm2-sclnc-owi-cm-20241106t234158-20241106t234314-00003-______ll_gd]
            split_coloc.sort()
            split_coloc = [
                x.replace("temp_placeholder_for_split", "______ll_gd")
                for x in split_coloc
            ]
            combination_id = "__".join(split_coloc)
            if combination_id not in combination_set:
                combination_set.add(combination_id)
            else:
                # Combination of files already processed, skip
                continue

            for other_mission_or_instr in other_missions_or_instr:
                # Other mission_or_instr can also be an instrument, to make instrument wide comparisons
                if other_mission_or_instr in cyclobs_config.INSTRUMENTS_SHORT:
                    # Get list of mission_or_instr that are in the checked instrument
                    other_missions_ok = cyclobs_config.INSTRUMENTS_MISSIONS_SHORT[
                        other_mission_or_instr
                    ]
                else:
                    other_missions_ok = [other_mission_or_instr]
                combo = f"{mission_or_instr}_{other_mission_or_instr}"
                ds = xr.open_dataset(file)
                mission1_short = cyclobs_config.MISSION_LONG_TO_SHORT[
                    ds.attrs["missionName_1"]
                ]
                mission2_short = cyclobs_config.MISSION_LONG_TO_SHORT[
                    ds.attrs["missionName_2"]
                ]
                if (
                    any(m == mission1_short for m in missions_ok)
                    or any(m == mission2_short for m in missions_ok)
                ) and (
                    any(m == mission1_short for m in other_missions_ok)
                    or any(m == mission2_short for m in other_missions_ok)
                ):
                    if combo not in files_dict:
                        files_dict[combo] = []
                    files_dict[combo].append(file)
                ds.close()

    # exemples of return
    # {"C-Band SAR_L-Band Radiometer": [filelist]}
    # {"SMAP_SMOS": [filelist], "SMAP_C-Band_SAR": [filelist], "SMAP_RS2": [filelist], etc}
    return files_dict


# Get all coloc filenames from a mission_or_instr in a dictionnary
# FIXME This function is not generic at all
def get_one_mission_coloc_datasets(path, mission=""):
    files_dict = {}
    # get smap coloc files
    if mission == "smap":
        smap_smos = []
        smap_sar = []
        smap_rs2 = []
        smap_s1a = []
        smap_s1b = []

        for file in os.listdir(path):
            if "smap" in file and ("rs2" in file or "s1a" in file or "s1b" in file):
                smap_sar.append(path + file)
            if "smap" in file and "rs2" in file:
                smap_rs2.append(path + file)
            elif "smap" in file and "s1a" in file:
                smap_s1a.append(path + file)
            elif "smap" in file and "s1b" in file:
                smap_s1b.append(path + file)
            elif "smap" in file and "SM_OPER_MIR" in file:
                smap_smos.append(path + file)

        files_dict = {
            "smap_sar": smap_sar,
            "smap_s1a": smap_s1a,
            "smap_rs2": smap_rs2,
            "smap_s1b": smap_s1b,
            "smap_smos": smap_smos,
        }

    # get smos coloc files
    elif mission == "smos":
        smos_sar = []
        smos_rs2 = []
        smos_s1a = []
        smos_s1b = []

        for file in os.listdir(path):
            if "SM_OPER_MIR" in file and (
                "rs2" in file or "s1a" in file or "s1b" in file
            ):
                smos_sar.append(path + file)
            if "SM_OPER_MIR" in file and "rs2" in file:
                smos_rs2.append(path + file)
            elif "SM_OPER_MIR" in file and "s1a" in file:
                smos_s1a.append(path + file)
            elif "SM_OPER_MIR" in file and "s1b" in file:
                smos_s1b.append(path + file)
        files_dict = {
            "smos_sar": smos_sar,
            "smos_s1a": smos_s1a,
            "smos_rs2": smos_rs2,
            "smos_s1b": smos_s1b,
        }

    # get rs2-s1 coloc files
    elif mission == "rs2":
        rs2_s1a = []
        rs2_s1b = []

        for file in os.listdir(path):
            if "rs2" in file and "s1a" in file:
                rs2_s1a.append(path + file)
            elif "rs2" in file and "s1b" in file:
                rs2_s1b.append(path + file)
        files_dict = {"rs2_s1a": rs2_s1a, "rs2_s1b": rs2_s1b}

    return files_dict


# For each colocation type, concatenate all files of this key to have only one dataset for each type
def concatenate_datasets_new(files_dict):
    dict_datasets = {}
    for key in files_dict:
        if len(files_dict[key]) == 0:
            continue

        mission1, mission2 = key.split("_")
        # SAR-SAR case
        if (
            mission1
            in cyclobs_config.INSTRUMENTS_MISSIONS_SHORT[
                cyclobs_config.SAR_INSTRUMENT_SHORT
            ]
            and mission2
            in cyclobs_config.INSTRUMENTS_MISSIONS_SHORT[
                cyclobs_config.SAR_INSTRUMENT_SHORT
            ]
        ):
            logger.info(f"Opening SAR-SAR coloc files: count {len(files_dict[key])}")
            df = pd.DataFrame()
            for f in files_dict[key]:
                ds_file = xr.open_dataset(f)
                df_file = ds_file.to_dataframe()
                df_file = df_file.loc[
                    (df_file["nrcs_cross_1"] > 0.00001)
                    & (df_file["nrcs_co_1"] > 0.00001)
                ]
                df_file = df_file.loc[
                    (df_file["nrcs_cross_2"] > 0.00001)
                    & (df_file["nrcs_co_2"] > 0.00001)
                ]
                df_file["missionName_1"] = ds_file.attrs["missionName_1"]
                df_file["missionName_2"] = ds_file.attrs["missionName_2"]
                df = pd.concat([df, df_file])

            # if dataset is rs2-s1a or rs2-s1b, inverting names, wind_speed and incidence_angle to use radarsat-2 incidence angle
            missionName1 = df["missionName_1"]
            missionName2 = df["missionName_2"]

            df["missionName_1"] = missionName2
            df["missionName_2"] = missionName1
            df = df.rename(
                columns={
                    "wind_speed_1": "wind_speed_2",
                    "wind_speed_2": "wind_speed_1",
                    "incidence_angle_1": "incidence_angle_2",
                    "incidence_angle_2": "incidence_angle_1",
                }
            )
        # L-Band case
        elif (
            mission1
            in cyclobs_config.INSTRUMENTS_MISSIONS_SHORT[
                cyclobs_config.LBAND_INSTRUMENT_SHORT
            ]
            and mission2
            in cyclobs_config.INSTRUMENTS_MISSIONS_SHORT[
                cyclobs_config.LBAND_INSTRUMENT_SHORT
            ]
        ):
            logger.info(f"Opening L-Band coloc files: count {len(files_dict[key])}")
            df = pd.DataFrame()
            for f in files_dict[key]:
                ds_file = xr.open_dataset(f)
                df_file = ds_file.to_dataframe()
                df_file["missionName_1"] = ds_file.attrs["missionName_1"]
                df_file["missionName_2"] = ds_file.attrs["missionName_2"]
                df = pd.concat([df, df_file])
        # Other (#FIXME that is SAR-other because incidence_angle_2 is being used)
        else:
            logger.info(f"Opening SAR-other coloc files: count {len(files_dict[key])}")
            df = pd.DataFrame()
            for f in files_dict[key]:
                ds_file = xr.open_dataset(f)
                df_file = ds_file.to_dataframe()
                df_file["missionName_1"] = ds_file.attrs["missionName_1"]
                df_file["missionName_2"] = ds_file.attrs["missionName_2"]
                df_file = df_file.loc[
                    (df_file["nrcs_cross_1"] > 0.00001)
                    & (df_file["nrcs_co_1"] > 0.00001)
                ]
                df = pd.concat([df, df_file])

            missionName1 = df["missionName_1"]
            missionName2 = df["missionName_2"]
            df["missionName_1"] = missionName2
            df["missionName_2"] = missionName1
            df = df.rename(
                columns={
                    "wind_speed_1": "wind_speed_2",
                    "wind_speed_2": "wind_speed_1",
                    "incidence_angle_1": "incidence_angle_2",
                }
            )

        # We have to define names manually for colocs with all sar and for cband-lband
        # so we can avoid having names of one specific mission_or_instr replacing "SAR"
        for instr in cyclobs_config.INSTRUMENTS_SHORT:
            instr1 = key.split("_")[0]
            instr2 = key.split("_")[1]
            if instr in instr1 or instr in instr2:
                m_short = cyclobs_config.INSTRUMENTS_MISSIONS_SHORT[instr]
                for mission_short in m_short:
                    m_long = cyclobs_config.MISSION_SHORT_TO_LONG[mission_short]
                    if df.iloc[0]["missionName_1"] == m_long:
                        df["missionName_1"] = instr
                    if df.iloc[0]["missionName_2"] == m_long:
                        df["missionName_2"] = instr

        # if key == f"{cyclobs_config.SMAP_SHORT}_{cyclobs_config.SAR_INSTRUMENT_SHORT}":
        #    ds['missionName1'] = cyclobs_config.SMAP_SHORT
        #    ds['missionName2'] = cyclobs_config.SAR_INSTRUMENT_SHORT
        # elif key == f"{cyclobs_config.SMOS_SHORT}_{cyclobs_config.SAR_INSTRUMENT_SHORT}":
        #    ds['missionName1'] = cyclobs_config.SMOS_SHORT
        #    ds['missionName2'] = cyclobs_config.SAR_INSTRUMENT_SHORT
        # elif key == f'{cyclobs_config.SAR_INSTRUMENT_SHORT}_{cyclobs_config.L}': #TODO
        #    ds["missionName1"] = "C-BAND"
        #    ds["missionName2"] = "L-BAND"

        dict_datasets[key] = df

    return dict_datasets


# For each colocation type, concatenate all files of this key to have only one dataset for each type
def concatenate_datasets(files_dict):
    dict_datasets = {}

    for key in files_dict:
        if len(files_dict[key]) == 0:
            continue
        if key.startswith("rs2"):
            ds = xr.open_mfdataset(
                files_dict[key],
                data_vars=("wind_speed_1", "wind_speed_2", "incidence_angle_1"),
                coords="minimal",
                compat="override",
                combine="nested",
                concat_dim="delta_time",
            )
            # if dataset is rs2-s1a or rs2-s1b, inverting names, wind_speed and incidence_angle to use radarsat-2 incidence angle
            missionName1 = ds.missionName_1
            missionName2 = ds.missionName_2
            ds["missionName_1"] = missionName2
            ds["missionName_2"] = missionName1
            ds = ds.rename(
                {
                    "wind_speed_1": "wind_speed_2",
                    "wind_speed_2": "wind_speed_1",
                    "incidence_angle_1": "incidence_angle_2",
                    "incidence_angle_2": "incidence_angle_1",
                }
            )
        elif key == "smap_smos":
            ds = xr.open_mfdataset(
                files_dict[key],
                data_vars=("wind_speed_1", "wind_speed_2"),
                coords="minimal",
                compat="override",
                combine="nested",
                concat_dim="delta_time",
            )
        else:
            ds = xr.open_mfdataset(
                files_dict[key],
                data_vars=("wind_speed_1", "wind_speed_2", "incidence_angle_2"),
                coords="minimal",
                compat="override",
                combine="nested",
                concat_dim="delta_time",
            )

        # We have to define names manually for colocs with all sar and for cband-lband
        # so we can avoid having names of one specific mission_or_instr replacing "SAR"
        if key == "smap_sar":
            ds["missionName_1"] = "SMAP"
            ds["missionName_2"] = "SAR"
        elif key == "smos_sar":
            ds["missionName_1"] = "SMOS"
            ds["missionName_2"] = "SAR"
        elif key == "cband_lband":
            ds["missionName_1"] = "C-BAND"
            ds["missionName_2"] = "L-BAND"
        dict_datasets[key] = ds

    return dict_datasets


# Create and save wind speed comparison plots
def build_comp_plots(dict_datasets, colormap_dict, export_path):
    for key in dict_datasets:
        # opening datasets
        df = dict_datasets[key]

        if len(df.index) == 0:
            logger.info(f"No data for {key}. No plot will be generated.")
            return

        mission_display_1, mission_display_2 = key.split("_")

        # getting mission_or_instr names
        mission1 = df.iloc[0]["missionName_1"]
        mission2 = df.iloc[0]["missionName_2"]

        if isinstance(mission1, str) == False:
            mission1 = str(mission1.values)
            mission2 = str(mission2.values)

        if mission1 in list(cyclobs_config.MISSION_LONG_TO_SHORT.keys()):
            mission1_short = cyclobs_config.MISSION_LONG_TO_SHORT[mission1]
        else:
            mission1_short = mission1

        if mission2 in list(cyclobs_config.MISSION_LONG_TO_SHORT.keys()):
            mission2_short = cyclobs_config.MISSION_LONG_TO_SHORT[mission2]
        else:
            mission2_short = mission2

        # keeping only not nan values

        not_nan = (~np.isnan(df["wind_speed_1"])) & (~np.isnan(df["wind_speed_2"]))
        df = df[not_nan]

        # calculating stats
        mean, stdv, nim, si, corr = do_stat(df["wind_speed_1"], df["wind_speed_2"])

        # plotting wind speed points
        points = hv.Scatter(
            (
                df["wind_speed_1"].clip(upper=200),
                df["wind_speed_2"].clip(upper=200),
            )
        )
        plot = (
            rasterize(points)
            .opts(
                title=mission_display_1 + " vs " + mission_display_2 + " \n wind speed",
                width=800,
                height=750,
            )
            .opts(
                xlabel=mission1_short + " wind speed (m/s)",
                ylabel=mission2_short + " wind speed (m/s)",
            )
            .opts(show_grid=True, colorbar=True, cmap=colormap_dict[key], toolbar=None)
        ) * hv.Curve([[0, 0], [80, 80]]).opts(
            line_dash="dashed", color="black", line_width=1
        )

        # stats panel
        txt = " Npts: {:d}\n Bias: {:4.2f} m/s\n Std: {:4.2f} m/s\n SI: {:4.2f}%\n R: {:4.2f}".format(
            nim, mean, stdv, si, corr
        )
        plot = plot * hv.Text(1, 78, txt, halign="left", valign="top", fontsize=15)

        # plotting percentile curve
        p = np.arange(100)
        psar = np.percentile(df["wind_speed_2"], p)
        pref = np.percentile(df["wind_speed_1"], p)
        plot = plot * hv.Curve((pref, psar)).opts(color="black", line_width=1)

        # plotting percentile points
        p = np.array([10, 25, 50, 75, 99])
        psar = np.percentile(df["wind_speed_2"], p)
        pref = np.percentile(df["wind_speed_1"], p)
        plot = plot * hv.Scatter((pref, psar)).opts(color="black", size=8)
        for pp, _p in enumerate(p):
            plot = plot * hv.Text(
                pref[pp] + 5, psar[pp], str(p[pp] / 100.0), fontsize=17
            ).opts(color="black")

        plot.opts(fontscale=2, fontsize={"title": "100%"})
        # saving plot
        # hv.save(plot, filename = export_path+mission1+"-"+mission2+"-comp.png", fmt="png", backend="bokeh")
        pane = pn.panel(plot)
        pane.save(
            os.path.join(
                export_path, mission_display_1 + "-" + mission_display_2 + "-comp.png"
            )
        )
        # image = get_screenshot_as_png(plot)
        # export_png(image, filename="plot.png")


# Create and save wind_speed comparison / SAR incidence angle plots
def build_comp_stats_plots(dict_datasets, colormap_dict, export_path):
    if "SMAP_SMOS" in dict_datasets.keys():
        dict_datasets.pop("SMAP_SMOS")
    if "SMOS_SMAP" in dict_datasets.keys():
        dict_datasets.pop("SMOS_SMAP")

    unit = "m/s"

    for key in dict_datasets:
        # opening datasets
        df = dict_datasets[key]

        if len(df.index) == 0:
            logger.info(f"No data for {key}. No plot will be generated.")
            return

        mission_display_1, mission_display_2 = key.split("_")

        # getting mission_or_instr names
        mission1 = df.iloc[0]["missionName_1"]
        mission2 = df.iloc[0]["missionName_2"]

        if isinstance(mission1, str) == False:
            mission1 = str(mission1.values)
            mission2 = str(mission2.values)

        if mission1 in list(cyclobs_config.MISSION_LONG_TO_SHORT.keys()):
            mission1_short = cyclobs_config.MISSION_LONG_TO_SHORT[mission1]
        else:
            mission1_short = mission1

        if mission2 in list(cyclobs_config.MISSION_LONG_TO_SHORT.keys()):
            mission2_short = cyclobs_config.MISSION_LONG_TO_SHORT[mission2]
        else:
            mission2_short = mission2

        not_nan = (
            ~pd.isna(df["incidence_angle_2"])
            & ~pd.isna(df["wind_speed_1"])
            & ~pd.isna(df["wind_speed_2"])
        )
        df = df[not_nan]

        # Calculate dynamic bins based on the data distribution
        bin_edges = np.histogram_bin_edges(df["incidence_angle_2"], bins="auto")

        inc_tab = (bin_edges[:-1] + bin_edges[1:]) / 2  # Midpoints of bins

        bias_tab = np.empty(len(inc_tab))
        stdv_tab = np.empty(len(inc_tab))
        bias_tab[:] = np.nan
        stdv_tab[:] = np.nan

        # variables init
        # inc_tab = np.arange(20, 50, 2.5)  # separating plot in 3 parts
        # bias_tab = inc_tab * 0 + np.nan
        # stdv_tab = inc_tab * 0 + np.nan
        # inc_tab_stat = np.array([25, 35, 45])

        # putting not nan values in variables
        __inc = df["incidence_angle_2"]
        __spd_sar = df["wind_speed_2"]
        __spd_ref = df["wind_speed_1"]

        # calculating bias and standard deviation for each part
        for cinc, _inc in enumerate(inc_tab):
            bin_width = bin_edges[cinc + 1] - bin_edges[cinc]
            filt = np.abs(_inc - __inc) < (bin_width / 2)
            if np.sum(filt) > 0:
                bias_tab[cinc] = np.nanmean(__spd_sar[filt] - __spd_ref[filt])
                stdv_tab[cinc] = np.nanstd(__spd_sar[filt] - __spd_ref[filt])

        # Dynamically determining inc_tab_stat points (example: 3 evenly spaced points)
        inc_tab_stat_indices = np.linspace(0, len(inc_tab) - 1, 4, dtype=int)
        inc_tab_stat = inc_tab[inc_tab_stat_indices]
        inc_tab_stat_2 = []
        for i in range(len(inc_tab_stat) - 1):
            inc_tab_stat_2.append(
                inc_tab_stat[i] + ((inc_tab_stat[i + 1] - inc_tab_stat[i]) / 2)
            )
        x_min, x_max = (
            np.nanquantile(df["incidence_angle_2"], 0.0001) - 3,
            np.nanquantile(df["incidence_angle_2"], 0.9999) + 3,
        )
        y_min, y_max = (
            np.nanquantile(df["wind_speed_2"] - df["wind_speed_1"], 0.0001) - 3,
            np.nanquantile(df["wind_speed_2"] - df["wind_speed_1"], 0.9999) + 3,
        )

        # plotting wind speed biais vs sar incidence angle (rs2 if multiple sar)
        points = hv.Scatter(
            (
                df["incidence_angle_2"],
                df["wind_speed_2"] - df["wind_speed_1"],
            )
        ).opts(xlim=(x_min, x_max), ylim=(y_min, y_max))
        plot = (
            rasterize(points)
            .opts(
                title=mission_display_1 + " vs " + mission_display_2,
                width=800,
                height=750,
                xlim=(x_min, x_max),
                ylim=(y_min, y_max),
            )
            .opts(
                xlabel=mission2_short + " incidence angle (deg)",
                ylabel=mission2_short + "-" + mission1_short + " wind speed (m/s)",
            )
            .opts(show_grid=True, colorbar=True, cmap=colormap_dict[key], toolbar=None)
        ) * hv.Curve([[10, 0], [55, 0]]).opts(
            line_dash="dashed", color="black", line_width=1
        )
        # plotting
        plot = (
            plot
            * hv.Scatter((inc_tab, bias_tab)).opts(color="black", line_width=2)
            * hv.Curve((inc_tab, bias_tab)).opts(color="black")
            * hv.Curve((inc_tab, bias_tab - stdv_tab)).opts(color="black")
            * hv.Curve((inc_tab, bias_tab + stdv_tab)).opts(color="black")
        )

        ## plotting separation lines and stats for each part
        # for cinc, _inc in enumerate(inc_tab_stat):
        #    filt = np.abs(_inc - __inc) < 5
        #    plot = plot * hv.VLine(_inc - 5).opts(
        #        color="black", line_width=2, line_dash="dashed"
        #    )
        #    mean, stdv, nim, si, corr = do_stat(__spd_ref[filt], __spd_sar[filt])
        #    txt = " Npts: {:d}\n Bias: {:4.2f} m/s\n Std: {:4.2f} m/s\n SI: {:4.2f}%\n R: {:4.2f}".format(
        #        nim, mean, stdv, si, corr
        #    )
        #    plot = plot * hv.Text(
        #        _inc, 18, txt, halign="center", valign="top", fontsize=15
        #    )

        for i in range(len(inc_tab_stat) - 1):
            bin_min = inc_tab_stat[i]
            bin_max = inc_tab_stat[i + 1]
            bin_middle = bin_min + (bin_width / 2)

            # Find the closest bin index in inc_tab for each _inc in inc_tab_stat
            bin_min_ok = bin_edges[np.abs(inc_tab - bin_min).argmin()]
            bin_max_ok = bin_edges[np.abs(inc_tab - bin_max).argmin()]
            bin_width = bin_max_ok - bin_min_ok
            bin_middle_ok = bin_min_ok + (bin_width / 2)
            filt = np.abs(bin_middle_ok - __inc) < (bin_width / 2)

            if np.sum(filt) > 0:
                plot = plot * hv.VLine(bin_min_ok).opts(
                    color="black", line_width=2, line_dash="dashed"
                )
                plot = plot * hv.VLine(bin_max_ok).opts(
                    color="black", line_width=2, line_dash="dashed"
                )
                mean, stdv, nim, si, corr = do_stat(__spd_ref[filt], __spd_sar[filt])
                txt = " Npts: {:d}\n Bias: {:4.2f} {:s}\n Std: {:4.2f} {:s}\n SI: {:4.2f}%\n R: {:4.2f}".format(
                    nim, mean, unit, stdv, unit, si, corr
                )
                y_text_pos = y_max - (y_max - y_min) * 0.05
                # if scatter_opts and "ylim" in scatter_opts:
                #    y_text_pos = scatter_opts["ylim"][1] - 2
                plot = plot * hv.Text(
                    bin_middle_ok,
                    y_text_pos,
                    txt,
                    halign="center",
                    valign="top",
                    fontsize=10,
                )

        plot.opts(fontscale=2, fontsize={"title": "100%"})

        # hv.save(plot, filename = export_path+mission1+"-"+mission2+"-inc_angle-comp.png", fmt="png", backend="bokeh")
        pane = pn.panel(plot)

        pane.save(
            os.path.join(
                export_path,
                mission_display_1 + "-" + mission_display_2 + "-inc_angle-comp.png",
            )
        )
        # export_png(plot, filename = export_path+mission1+"-"+mission2+"-inc_angle-comp.png")


# get plots colormaps
def get_colormap(
    ds_names, colors="same", colormap=["autumn", "kb_r", "kr_r", "kg_r", "autumn"]
):
    dict_colormap = {}
    for ds in ds_names:
        # same color for all plots
        if colors == "same":
            try:
                if isinstance(colormap, str):
                    dict_colormap[ds] = colormap
                else:
                    dict_colormap[ds] = colormap[0]
            except:
                print(
                    "Error : colormap is not valid, replacing it by default colormap : 'autumn'"
                )
                dict_colormap[ds] = "autumn"

        # different colors
        elif colors == "different":
            try:
                dict_colormap[ds] = colormap[list(ds_names).index(ds)]
            except:
                print(
                    "Error : colormap default lenght is 5, if you have more datasets, please add colormaps. Replacing colormap by default value : 'autumn' for next plots"
                )
                dict_colormap[ds] = "autumn"

    return dict_colormap
