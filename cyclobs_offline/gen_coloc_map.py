from fileinput import filename
from bokeh.palettes import Category10
import panel as pn
import re
import geopandas as gpd
from geo_shapely import smallest_dlon
import shapely
import cartopy.crs as ccrs
import holoviews as hv
import geoviews as gv
import geoviews.feature as gf
from shapely.geometry import Polygon, LineString
import os

gv.extension("bokeh")

from cyclobs_offline.gen_coloc_stats_plots import open_colocs
import logging

# Configure logger
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


# Function to convert a Linestring to a Polygon by connecting end to start
def linestring_to_polygon(geometry):
    if isinstance(geometry, LineString):
        # Append the start point at the end of the points list to close the polygon
        points = list(geometry.coords)
        if points[0] != points[-1]:  # Check if the Linestring is not already closed
            points.append(points[0])  # Make the Linestring a closed loop
        return Polygon(points)
    else:
        return geometry


# Create coloc acquisitions maps
def createMap(title, gdf, legend, force_points=False, opts={}):
    # Useful variables
    if opts is None:
        opts = {}
    lonOffset = -160
    targetProjection = ccrs.PlateCarree(central_longitude=lonOffset)

    if len(gdf.index) == 0:
        plot = (
            gf.land
            * gf.ocean
            * gf.coastline.opts(
                projection=targetProjection,
                width=1200,
                height=900,
                padding=0.1,
                axiswise=True,
                title=title,
                tools=["hover"],
                global_extent=True,
            )
        )

    # Set Coordinate Reference Systems
    # default crs from input, it's the full def of 4326
    crs_in = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"
    # converting tracks and crs to proj.proj4_init give this error for pyproj<=2.0.0
    # so we change the prime meridian (+pm), to match lonOffset in PlateCarre
    crs_out = "%s +pm=%d" % (crs_in, lonOffset)
    gdf.crs = crs_in
    gdf = gdf.to_crs(crs=crs_out)
    gdf["geometry"] = gdf["geometry"].apply(linestring_to_polygon)

    # Create a list of polygon footprints to show all Acquisitions on Map
    if len(gdf.index) > 1200 or force_points:
        gdf["geometry"] = gdf.centroid
        footprints = [
            gv.Points(
                gdf.loc[gdf[legend] == l],
                vdims=["color", legend],
                crs=targetProjection,
                label=l,
            )
            for l in gdf[legend].unique()
        ]
        # Transform this list of polygon footprints in "holoview plot overlay objects"
        list_footprint_plots = [
            ft.opts(
                color="color",
                tools=["hover", "wheel_zoom"],
                legend_position="top_right",
                show_legend=True,
                size=5,
            )
            for ft in footprints
        ]
    else:
        footprints = [
            gv.Polygons(
                gdf.loc[gdf[legend] == l],
                vdims=["color", legend],
                crs=targetProjection,
                label=l,
            )
            for l in gdf[legend].unique()
        ]
        # Transform this list of polygon footprints in "holoview plot overlay objects"
        list_footprint_plots = [
            ft.opts(
                fill_color="color",
                tools=["hover", "wheel_zoom"],
                legend_position="top_right",
                show_legend=True,
            )
            for ft in footprints
        ]

    # list_footprint_plots = [ft.opts(tools=["hover", "wheel_zoom"]) for ft in footprints]

    #  Compute all the "holoview plot overlay objects" to multiply them with the other context overlay to be displayed
    len_list = len(list_footprint_plots)
    pre_plot_polygons = list_footprint_plots[0]
    if len_list > 1:
        for i in range(len_list - 1):
            pre_plot_polygons = pre_plot_polygons * list_footprint_plots[i + 1]

    # Create final plot to draw the Map
    plot = (
        gf.land
        * gf.ocean
        * gf.coastline.opts(
            projection=targetProjection,
            width=1200,
            height=900,
            padding=0.1,
            axiswise=True,
            title=title,
            tools=["hover"],
        )
        * pre_plot_polygons
    ).opts(**opts)

    return plot
    # plot = plot.redim.range(Latitude=(-70, 70))
    # Code to set axis right no matter what is the longitude offset
    lonAxisTransformCode = """
    if (tick <= -20) {
        return -(360+lonOffset) - tick;
    } else {
        return -lonOffset - tick;
    }
    """


# creating map
def acqMap(
    mission, mission_files, export_path, export_id, title="Acquisition footprints"
):
    # Get all the datasets from one mission_or_instr
    datasets = sum(
        [open_colocs(filelist=mission_files[key]) for key in mission_files.keys()], []
    )
    # Get missions names
    list_missions = sum(
        [
            [re.sub(f"_?{mission}_?", "", key).upper()] * len(mission_files[key])
            for key in mission_files
        ],
        [],
    )
    # Get colors to display footprints
    list_mission_color = sum(
        [
            [Category10[10][i]] * len(list(mission_files.values())[i])
            for i in range(len(mission_files.keys()))
        ],
        [],
    )

    count = len(datasets)
    title = title + " (" + str(count) + " co-locations)"

    # Get all colocs common footprints from the datasets of the mission_or_instr
    footprints = [shapely.wkt.loads(ds.polygon_common_zone) for ds in datasets]
    data = {
        "geometry": footprints,
        "color": list_mission_color,
        "mission_or_instr": list_missions,
    }

    gdf = gpd.GeoDataFrame(data)

    # If splitting geoms is necessary just do this
    gdf["geometry"] = gdf["geometry"].apply(smallest_dlon)
    gdf = gdf.explode(index_parts=True)

    if len(gdf.index) == 0:
        logger.warning(f"No data to create map for {mission}")
        return

    # Create map
    plot = createMap(
        title,
        gdf,
        legend="mission_or_instr",
        opts={"ylim": (-70, 80), "xlim": (-180, 180)},
    )
    # bokeh_pane = pn.pane(plot)

    subdir = "maps"
    os.makedirs(os.path.join(export_path, subdir), exist_ok=True)
    # Saving map as an image
    hv.save(
        plot, filename=os.path.join(export_path, subdir, "map-" + export_id + ".png")
    )
