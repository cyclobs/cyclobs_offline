from fileinput import filename
import os
import xarray as xr
import pandas as pd

from math import pi
import numpy as np

from bokeh.palettes import Category10, Category20
from bokeh.plotting import figure, show
from bokeh.transform import cumsum, factor_cmap
from bokeh.io import export_png
from bokeh.models import ColumnDataSource, FactorRange

import holoviews as hv

import re


import collections
from datetime import datetime


# Open all files of a repertory or a filelist, and returning a list of datasets
def open_colocs(path="", filelist="", df_api=None):
    datasets = []
    api_lines = []
    if filelist != "":
        for file in filelist:
            if df_api is not None:
                match_line = df_api.loc[df_api["coloc_file"] == file]
                if len(match_line.index) > 0:
                    api_lines.append(match_line.iloc[0])
                    datasets.append(xr.open_dataset(file))
            else:
                datasets.append(xr.open_dataset(file))
    elif path != "":
        for file in os.listdir(path):
            if df_api is not None:
                match_line = df_api.loc[df_api["coloc_file"] == path + file]
                if len(match_line.index) > 0:
                    api_lines.append(match_line.iloc[0])
                    datasets.append(xr.open_dataset(path + file))
            else:
                datasets.append(xr.open_dataset(path + file))
    if df_api is not None:
        return datasets, api_lines
    else:
        return datasets


# Create pie charts
# Creates a pie chart filled with the given dataDict
# - dataDict must be formatted as follow:
#         { 'key1' : value, 'key2': value}
# - indexName is string labeling the showed data
# https://bokeh.pydata.org/en/latest/docs/gallery/pie_chart.html
def createPieChart(title, dataDict, indexName, color="color"):
    # Create the pandas serie corresponding to a list of all unique informations to plot
    data = (
        pd.Series(dataDict)
        .reset_index(name="value")
        .rename(columns={"index": indexName})
    )

    # Data for the angle
    data["angle"] = data["value"] / data["value"].sum() * 2 * pi
    # Data for the color
    if len(dataDict) > 2:
        data["color"] = Category20[len(dataDict)]
    else:
        data["color"] = Category20[3][: len(dataDict)]
    # Data for the legend
    data["legend_group"] = [
        "{} : {} ({}%)".format(
            instr,
            round(data["value"][i]),
            int(round((data["value"][i] / data["value"].sum()) * 100)),
        )
        for i, instr in enumerate(data[indexName])
    ]

    # Create the Bokeh plot
    p = figure(
        title=title,
        height=250,
        width=480,
        x_range=(-1, 1),
        y_range=(0.5, 1.5),
        tools="hover",
        tooltips="@{}: @value".format(indexName),
        toolbar_location=None,
    )

    # Create the wedge objects to fill the pie chart
    r = p.wedge(
        source=data,
        legend_group="legend_group",
        x=-0.5,
        y=1,
        radius=0.4,
        start_angle=cumsum("angle", include_zero=True),
        end_angle=cumsum("angle"),
        line_color="white",
        fill_color=color,
    )

    # Specify plot configuration
    p.axis.axis_label = None
    p.axis.visible = False
    p.grid.grid_line_color = None

    return p


# Create basins pie charts
def create_basin_charts(df_api, export_path, title, filename):
    # Define color for each basin
    basinsColor = {
        "NA": "gray",
        "NEP": "blue",
        "NI": "red",
        "NWP": "orange",
        "SI": "yellow",
        "SP": "green",
    }

    # Get basins of all collocations
    basins = df_api["basin"].tolist()
    dict_basins = collections.Counter(basins)

    # Get colors
    color = factor_cmap(
        "basins", palette=list(basinsColor.values()), factors=list(basinsColor.keys())
    )

    # Create basins chart and saving it to png format
    plot = createPieChart(title, dict_basins, "basins", color=color)
    subdir = os.path.join("charts", "basins")
    os.makedirs(os.path.join(export_path, subdir), exist_ok=True)
    export_png(plot, filename=os.path.join(export_path, subdir, filename + ".png"))
    # show(plot)


# Create categories pie charts
def create_categories_charts(df_api, export_path, title, filename):
    # Define color for each category
    stormsColor = {
        "storm": "gray",
        "cat-1": "blue",
        "cat-5": "red",
        "cat-4": "orange",
        "cat-3": "yellow",
        "cat-2": "green",
        "dep": "cyan",
    }

    # Get categories of all collocations
    categs = df_api["curr_cyc_categ"].tolist()
    dict_categories = collections.Counter(categs)

    # Get colors
    color = factor_cmap(
        "categories",
        palette=list(stormsColor.values()),
        factors=list(stormsColor.keys()),
    )

    # Create categories chart and saving it to png format
    plot = createPieChart(title, dict_categories, "categories", color=color)
    subdir = os.path.join("charts", "categories")
    os.makedirs(os.path.join(export_path, subdir), exist_ok=True)
    export_png(plot, filename=os.path.join(export_path, subdir, filename + ".png"))
    # show(plot)


# Prepare data to create acquisitions years bar plots
def yearsBarChart(mission, mission_files, export_path, export_id, title="title"):
    # { "years": ["2018", "2019"],
    #   "RADARSAT-2": [10, 17]
    #   "SENTINEL-1 A" : [5, 9] }

    df_dates = []
    df_missions = []
    # Get year of each collocation
    for key in mission_files:
        ds = open_colocs(filelist=mission_files[key])
        dates = []
        for dataset in ds:
            if "measurementDate_1" in dataset.attrs:
                date_t = datetime.strptime(
                    dataset.attrs["measurementDate_1"], "%Y-%m-%dT%H:%M:%SZ"
                )
                dates.append(str(date_t.year))
            elif "measurementStartDate_1" in dataset.attrs:
                date_t = datetime.strptime(
                    dataset.attrs["measurementStartDate_1"], "%Y-%m-%d %H:%M:%S"
                )
                dates.append(str(date_t.year))

        df_dates.append(dates)
        df_missions.append(
            [re.sub(f"_?{mission}_?", "", key).upper() for i in range(len(dates))]
        )

    df_dates = sum(df_dates, [])
    df_missions = sum(df_missions, [])
    df = pd.DataFrame({"years": df_dates, "mission_short": df_missions})
    # Count the number of collocation for each year
    counts = df.groupby(["years", "mission_short"]).size().reset_index(name="counts")

    title = title + " (" + str(len(df_dates)) + " co-locations)"

    subdir = os.path.join("charts", "acquisition-years")
    os.makedirs(os.path.join(export_path, subdir), exist_ok=True)
    # Create chart and save it to png
    return holoTimeBarChart(
        title,
        df=counts,
        kdims=["years", "mission_short"],
        filename=os.path.join(
            export_path, subdir, "acquisition-years-" + export_id + ".png"
        ),
    )


# Create years bar chart and save it to png
def holoTimeBarChart(title, df, kdims, filename):
    plot = (
        hv.Bars(df, kdims=kdims)
        .aggregate(function=np.sum)
        .opts(
            title=title,
            width=1000,
            show_grid=True,
            xrotation=90,
            tools=["hover"],
            show_legend=False,
        )
    )

    return hv.save(plot, filename)
