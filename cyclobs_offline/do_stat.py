import numpy as np
import cmath # complex math

# Calculate some stats for coloc stats plots
def do_stat(hs1,hs2,angle=False):
   # function that calculate the mean, standard deviation,number of points, scatter index and correlation coefficient
   # from the data of 2 arrays 
    if (angle==False):
        #mean and standard deviation
        mean = np.nanmean(hs2-hs1)
        stdv = np.nanstd(hs2-hs1)
        
        #filtering to keep only not nan values
        filt = ~np.isnan(hs1-hs2)

        #number of not nan points
        nim  = np.sum(filt)

        #scatter index and correlation coefficient
        xr = hs1[filt]
        xs = hs2[filt]
        a = xs-np.nanmean(xs)
        b = xr-np.nanmean(xr)
        si = 100*np.sqrt(np.sum(np.power((a-b),2.))/nim)/np.nanmean(xr)
        corr = np.corrcoef(xr,xs)[1,0]

    else:
        #filtering to keep only not nan values
        filt = ~np.isnan(hs1-hs2)
        
        #number of not nan points
        nim  = np.sum(filt)
        hs1=hs1[filt]
        hs2=hs2[filt]


        list_of_angles = (((hs2-hs1) %360)*180./np.pi).tolist()
        vectors= [cmath.rect(1, angle) for angle in list_of_angles]
        vector_sum= sum(vectors)        
        mean = cmath.phase(vector_sum)*180./np.pi
        
        list_of_angles = ((((hs2-hs1-mean)%360)*180./np.pi)**2.).tolist()
        vectors= [cmath.rect(1, angle) for angle in list_of_angles]
        vector_sum= sum(vectors)
        stdv = cmath.phase(vector_sum)*180./np.pi
        si=0
        corr=0



    return mean, stdv, nim, si, corr