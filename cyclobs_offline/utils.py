import logging
import requests
from urllib.parse import quote

logger = logging.getLogger(__name__)


def get_track_sources(api_host, commit=None, config=None, listing=None):
    params = {}
    # Quoting not needed because requests does it
    if commit is not None:
        params["commit"] = commit
    if config is not None:
        params["config"] = config
    if listing is not None:
        params["listing"] = listing

    req = requests.Request('GET', f"{api_host}/app/api/track_sources", params=params)
    prepared = req.prepare()
    print(f"URL: {prepared.url}")
    response = requests.Session().send(prepared)
    if response.status_code == 200:
        return response.text.split(",")
    else:
        error_msg = f"Failed to get track sources from API: {response.status_code}"
        logger.error(error_msg)
        raise RuntimeError(error_msg)
