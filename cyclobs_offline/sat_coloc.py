import logging
import os.path

import colormap_ext
import pathurl

import cartopy.crs as ccrs
import geoviews as gv

import holoviews as hv
from cyclobs_httpd.constants import SAR_STR
from cyclobs_utils import DataFrameDs

gv.extension("bokeh")
hv.extension("bokeh")
import numpy as np
import matplotlib.cm as cm
import pandas as pd
import panel as pn

from bokeh.embed import components
from bokeh.io import curdoc
from holoviews.selection import link_selections

from cyclone_formatter import readFormatted
from shapely.wkt import loads
from urllib.parse import quote


logger = logging.getLogger(__name__)


# Create an image plot for the files concerning a colocation
def createImagesPlots(
    ds, ls, dict_axis, bounding_boxes, colorMap=cm.get_cmap("high_wind_speed")
):
    # Create the images' plots, using the Colormap defined before
    list_plots = [
        (
            ls(
                hv.Image(ds, vdims=value)
                .opts(title=key, width=450, height=450)
                .opts(cmap=colorMap, show_grid=True, colorbar=True, clim=(0, 80))
                .opts(tools=["box_select", "tap", "wheel_zoom", "hover"])
            )
            * hv.Polygons(loads(bounding_boxes[key])).opts(
                fill_alpha=0, selection_fill_alpha=0
            )
        )
        for key, value in dict_axis.items()
    ]
    return list_plots


# Create plots analysing the data between the files of a colocation
def createAnalysisPlot(ds, ls, dict_axis):
    list_plots = []
    for index, key in enumerate(list(dict_axis.keys())):
        # Create a line to be displayed in the scatter plot
        if key == "Wind speed comparison":
            curve = hv.Curve([[0, 0], [80, 80]], dict_axis[key][0], dict_axis[key][1])
        else:
            curve = hv.Curve([[15, 0], [50, 0]], dict_axis[key][0], dict_axis[key][1])
        curve = curve.opts(line_dash="dashed", color="black")

        # Create the plot to be added
        plot = (
            (
                ls(
                    hv.Scatter(ds, dict_axis[key][0], dict_axis[key][1]).opts(
                        tools=["tap", "box_select", "wheel_zoom", "hover"]
                    )
                )
                * curve
            )
            .opts(title=key, width=450, height=450, show_grid=True)
            .opts(tools=["box_select", "wheel_zoom", "hover", "tap"])
        )
        if key == "Wind speed comparison":
            plot.opts(xlim=(0, 80), ylim=(0, 80))
        else:
            plot.opts(xlim=(15, 50), ylim=(-25, 25))

        list_plots.append(plot)
    return list_plots


# Transform images and analysis plots to Bokeh's html content to display it in the template
def createBokehPlot(plots):
    divsScripts = [components(plots)]

    divStyles = ['<div class="col s12" style="display:inline-block;">']

    sep = list(zip(*divsScripts))

    return sep, divStyles


def computeStepResolution(datasets):
    for d in datasets:
        deg_per_index = abs(d["x"][0] - d["x"][1])
        # a and b coeffs have been computed using SMOS (0.25) and SAR deg per index
        # and using a step of 1 for SMOS and 4 for SAR
        # a = -12.460620804
        # b = 4.115155201

        # step 1 for SMOS and step 10 for SAR
        a = -36
        b = 10
        step = round(float(a * deg_per_index + b))

        return step


# Create an Holoviews container for a colocation of two acquisitions - (or more but it is not fully implemented yet)
def getContainer(
    df_ds,
    isSAR,
    list_missions,
    list_missions_short,
    dict_vdims={"wind_speed": "wind_speed (m/s)"},
):
    # Get the dataset for each unique file
    # Not using unique because "dataset" column is not hashable
    # init_list_datasets = [df.loc[df["data_url"] == filename]["dataset"].tolist()[0]
    #                      for filename in df.data_url.unique().tolist()]
    init_list_datasets = [
        df_ds.loc_ds(df_ds.df["data_url"] == filename)[0]
        for filename in df_ds.df.data_url.unique().tolist()
    ]

    # # Get the bounding box (lon/lat) - coordinate's maximum and minimum
    # images_bounding_box = {
    #     "X_max" : max([math.ceil(float(max(ds.x).values)) for ds in init_list_datasets]),
    #     "X_min" : min([math.floor(float(min(ds.x).values)) for ds in init_list_datasets]),
    #     "Y_max" : max([math.ceil(float(max(ds.y).values)) for ds in init_list_datasets]),
    #     "Y_min" : min([math.floor(float(min(ds.y).values)) for ds in init_list_datasets])
    # }

    # Reproject each acquisition to match the resolution, projection, and region of the others
    list_datasets = init_list_datasets
    for index, dataset in enumerate(list_datasets):
        for i in range(len(list_datasets) - 1):
            if index + i < len(list_datasets) - 1:
                list_datasets[index] = dataset.rio.reproject_match(
                    list_datasets[index + i]
                )
            else:
                list_datasets[index] = dataset.rio.reproject_match(list_datasets[0])

    # Associate each vdim to an holoviews dataset in a dictionary, and to two axis in another dictionary
    dict_axis = {}
    dict_ds = {}
    for key in dict_vdims.keys():
        dict_axis[key] = [
            ("{m} {vdim_value}".format(m=mission, vdim_value=dict_vdims[key]))
            for mission in list_missions_short
        ]

        step = computeStepResolution(list_datasets)

        # Transform the wind_speed's dataset into a 3D array - Prepare the wind speed to be plotted
        dict_ds[key] = [
            dataset[key].squeeze()[::step, ::step] for dataset in list_datasets
        ]

        # Suppress the values located outside the intersection
        # FIXME : Determine a mask, even if more than two acquisitions are concerned by the same colocation
        mask = np.isnan(dict_ds[key][0].values) | np.isnan(dict_ds[key][1].values)
        for index, dataset in enumerate(dict_ds[key]):
            dict_ds[key][index].values[mask] = np.nan

    # for key, list_datasets in dict_ds.items():
    #     for index, ds in enumerate(list_datasets):
    #         print("Dataset : index {i} for the vdim {k}\n{d}\n\n".format(i=index,k=key,d=ds))

    # FIXME : Create a container, even if more than two acquisitions are concerned by the same colocation
    # Create a container with the wind speed and incidence angle data, if all acquisition are from SAR
    if isSAR:
        list_vdims = [
            dict_axis["wind_speed"][0],
            dict_axis["wind_speed"][1],
            "{list_m} {vdim_value}".format(
                list_m=" - ".join(list_missions), vdim_value=dict_vdims["wind_speed"]
            ),
            "{list_m} {vdim_value}".format(
                list_m=" - ".join(reversed(list_missions)),
                vdim_value=dict_vdims["wind_speed"],
            ),
            dict_axis["incidence_angle"][0],
            dict_axis["incidence_angle"][1],
            dict_axis["elevation_angle"][0],
            dict_axis["elevation_angle"][1],
        ]

        ds = hv.Dataset(
            (
                dict_ds["wind_speed"][0].x,
                dict_ds["wind_speed"][0].y,
                dict_ds["wind_speed"][0].values,
                dict_ds["wind_speed"][1].values,
                dict_ds["wind_speed"][0].values - dict_ds["wind_speed"][1].values,
                dict_ds["wind_speed"][1].values - dict_ds["wind_speed"][0].values,
                dict_ds["incidence_angle"][0].values,
                dict_ds["incidence_angle"][1].values,
                dict_ds["elevation_angle"][0].values,
                dict_ds["elevation_angle"][1].values,
            ),
            ["x", "y"],
            vdims=list_vdims,
        )
    # Else, use only the wind speed data
    else:
        list_vdims = [dict_axis["wind_speed"][0], dict_axis["wind_speed"][1]]

        ds = hv.Dataset(
            (
                dict_ds["wind_speed"][0].x,
                dict_ds["wind_speed"][0].y,
                dict_ds["wind_speed"][0].values,
                dict_ds["wind_speed"][1].values,
            ),
            ["x", "y"],
            vdims=list_vdims,
        )

    # Determine informations analysis for the wind speed
    dict_ws_analysis = {}
    # Determine the bias  # (m/s)
    # https://numpy.org/doc/stable/reference/generated/numpy.nanmean.html#numpy.nanmean
    dict_ws_analysis["Bias"] = np.nanmean(
        dict_ds["wind_speed"][0].values - dict_ds["wind_speed"][1].values
    )
    # Determine the standard deviation  # (m/s)
    # https://numpy.org/doc/stable/reference/generated/numpy.nanstd.html#numpy.nanstd
    dict_ws_analysis["Standard deviation"] = np.nanstd(
        dict_ds["wind_speed"][0].values - dict_ds["wind_speed"][1].values
    )

    return ds, list_vdims, dict_ws_analysis


def readFilesInDataframe(df):
    # Initialize projection
    # projection = ccrs.Mercator()
    projection = ccrs.PlateCarree()

    # # Destination projection, as cartopy object (needed for Geoviews)
    # lonOffset = -160
    # projection = ccrs.PlateCarree(lonOffset)  # "EPSG:4326"
    #
    # # Default crs from input. It is the full definition of 4326 / PlateCarree
    # crs_in = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"
    # # Add a crs projection to the GeoDataFrame
    # gdf.crs = crs_in  # init by PlateCarree "EPSG:4326" -> to Merkator "EPSG:3395"
    #
    # # Converting acquisition to proj.proj4_init give an error for pyproj<=2.0.0
    # # so we change the prime meridian (+pm), to match lonOffset in PlateCarre
    # crs_out = "{crs_in} +pm={lonOffset}".format(crs_in=crs_in, lonOffset=lonOffset)

    # Transform url to local path
    df["data_url"] = df["data_url"].apply(lambda x: pathurl.toPath(x, schema="dmz"))
    df_ds = DataFrameDs(df)
    df_ds_dict = {}

    # FIXME - read data using only readFormatted() function from cyclone_formatter package
    # df["dataset"] = [readFormatted(row["data_url"]) for idx, row in df.iterrows()]

    # Read gridded data if both acquisition's instrument are SAR
    isSAR = all(instr == SAR_STR for instr in df.instrument_short.unique().tolist())
    if isSAR:
        #      df["dataset"] = [readFormatted(row["data_url"]).rename(name_dict={'lon': 'x', 'lat': 'y'})
        #                       for idx, row in df.iterrows()]
        df_ds_dict = {
            idx: readFormatted(row["data_url"]).rename(
                name_dict={"lon": "x", "lat": "y"}
            )
            for idx, row in df.iterrows()
        }

        # .rio.reproject(projection.proj4_params)
        # .rio.set_spatial_dims('lon','lat', inplace=True)
    else:
        #        df["dataset"] = [readFormatted(row["data_url"]).rename(name_dict={'lon': 'x', 'lat': 'y'})
        #                        for idx, row in df.iterrows()]
        df_ds_dict = {
            idx: readFormatted(row["data_url"]).rename(
                name_dict={"lon": "x", "lat": "y"}
            )
            for idx, row in df.iterrows()
        }

    df_ds.set_ds(df_ds_dict)
    # for row_index, row in df.iterrows():
    #     print("Dataset url : {}\n".format(os.path.basename(row["data_url"])))
    #     testDataset = row["dataset"]
    #     print("Test Dataset : {}\n".format(testDataset))
    #     fieldNames = list(row["dataset"].keys())
    #     print("Field names : {}\n".format(fieldNames))

    return df_ds, isSAR


# Get an holoviews/geoviews plotable object
def renderLayout(list_plots, isServer=True, analysis_panel=None):
    layout = hv.Layout(list_plots).cols(2).opts(merge_tools=False)

    # Ignore numpy warningstools
    np.warnings.filterwarnings("ignore")

    if isServer:
        # link = link_selections(layout, unselected_color='#505050', unselected_alpha=0.05). \
        #     opts(
        #     hv.opts.Image(active_tools=['pan', 'tap', 'wheel_zoom', 'hover']),
        #     hv.opts.Scatter(active_tools=['pan', 'tap', 'wheel_zoom', 'hover'],
        #                     nonselection_color='grey', nonselection_alpha=0.05)
        # )
        layout = layout.opts(
            hv.opts.Image(tools=["hover", "tap"]),
            hv.opts.Scatter(tools=["hover", "tap"]),
        )

        if analysis_panel is not None:
            row = pn.Row(layout, analysis_panel)
        else:
            row = pn.Row(layout)

        # row.css_classes = ["pn_root"]

        return curdoc().add_root(row.get_root(curdoc()))

    else:
        # , selection_mode='union'
        layout = layout.opts(
            hv.opts.Image(active_tools=["pan", "tap", "wheel_zoom", "hover"]),
            hv.opts.Scatter(
                active_tools=["pan", "tap", "wheel_zoom", "hover"],
                nonselection_color="grey",
                nonselection_alpha=0.05,
            ),
        )

        renderer = hv.renderer("bokeh")  # .instance(mode='server')
        plot = renderer.get_plot(layout).state

        return plot


def sat_coloc_plots_png(
    coloc_filename_1, coloc_filename_2, path, couple, cyclone_name, acq_date
):
    # Retrieving arguments passed from Flask
    # args = curdoc().session_context.request.arguments
    # coloc_filename_1 = args.get('coloc_filename_1')[0].decode('utf-8')
    # coloc_filename_2 = args.get('coloc_filename_2')[0].decode('utf-8')

    # Request the informations for this colocation
    request_url = (
        f"https://cyclobs.ifremer.fr/app/api/getData?include_cols=all&coloc_input={quote(coloc_filename_1)}&"
        f"coloc_comparison={quote(coloc_filename_2)}"
    )
    df_request = pd.read_csv(request_url, keep_default_na=False)
    df_request["order"] = 0
    df_request["order"] = df_request.apply(
        lambda x: 0 if x["mission_short"] == couple[0] else 1, axis=1
    )
    df_request = df_request.sort_values(by=["order"])

    df_ds_dataset, isSAR = readFilesInDataframe(df_request)

    list_missions = df_ds_dataset.df["mission_or_instr"].unique()
    list_short_missions = df_ds_dataset.df["mission_short"].unique()

    # Create an intermediate data format, a Dataset container used by Holoviews to create a plot
    if isSAR:
        dict_vdims = {
            "wind_speed": "wind speed (m/s)",
            "incidence_angle": "incidence angle",
            "elevation_angle": "elevation angle",
        }
    else:
        dict_vdims = {"wind_speed": "wind speed (m/s)"}

    ds, list_vdims, dict_ws_analysis = getContainer(
        df_ds_dataset, isSAR, list_missions, list_short_missions, dict_vdims
    )

    # Create the images plots
    dict_images_axis = {
        "{} wind speed (m/s)".format(list_short_missions[0]): list_vdims[0],
        "{} wind speed (m/s)".format(list_short_missions[1]): list_vdims[1],
    }

    bounding_boxes = {
        f"{list_short_missions[0]} wind speed (m/s)": df_ds_dataset.df.loc[
            df_ds_dataset.df["mission_short"] == list_short_missions[0], "bounding_box"
        ].iloc[0],
        f"{list_short_missions[1]} wind speed (m/s)": df_ds_dataset.df.loc[
            df_ds_dataset.df["mission_short"] == list_short_missions[1], "bounding_box"
        ].iloc[0],
    }

    ls = link_selections.instance()

    images_plots = createImagesPlots(
        ds, ls, dict_axis=dict_images_axis, bounding_boxes=bounding_boxes
    )

    # Create the analysis plots
    if isSAR:
        # Plotting also some scatter plots using :
        # - as x axis the incidence/elevation angle,
        # - as y axis the difference between wind speed for the two acquisition
        dict_analysis_axis = {
            "Wind speed comparison": [list_vdims[0], list_vdims[1]],
            "Bias {m1}-{m2} vs {m1} incidence angle".format(
                m1=list_short_missions[0], m2=list_short_missions[1]
            ): [list_vdims[4], list_vdims[2]],
            "Bias {m2}-{m1} vs {m2} incidence angle".format(
                m1=list_short_missions[0], m2=list_short_missions[1]
            ): [list_vdims[5], list_vdims[3]],
        }
        # FIXME : Add Radarsat-2 plot for elevation angle, once this information is added to it's files
        if list_missions[0] != "RADARSAT-2":
            dict_analysis_axis[
                "Bias {m1}-{m2} vs {m1} elevation angle".format(
                    m1=list_short_missions[0], m2=list_short_missions[1]
                )
            ] = [list_vdims[6], list_vdims[2]]
        else:
            dict_analysis_axis[
                "Bias {m2}-{m1} vs {m2} elevation angle".format(
                    m1=list_short_missions[0], m2=list_short_missions[1]
                )
            ] = [list_vdims[7], list_vdims[3]]
    else:
        dict_analysis_axis = {"Wind speed comparison": [list_vdims[0], list_vdims[1]]}
    analysis_plots = createAnalysisPlot(ds, ls, dict_axis=dict_analysis_axis)

    windSpeedInfosMarkdown = f"""
        ##### Analysis informations on the wind speed (m/s)
        ###### {list(dict_ws_analysis.keys())[0]} : {list(dict_ws_analysis.values())[0]:.5f}
        ###### {list(dict_ws_analysis.keys())[1]} : {list(dict_ws_analysis.values())[1]:.5f}
        """
    # windSpeedInfosMarkdown = "\n".join(
    #     ["Analysis informations on the wind speed (m/s)"] +
    #     ["{k:.5f} - {v:.5f}".format(k=key, v=value) for key, value in dict_ws_analysis.items()]
    # )

    pnWindSpeedInfos = pn.pane.Markdown(
        windSpeedInfosMarkdown,
        style={"text-align": "center", "width": "100%"},
        margin=(10, 30),
        align="start",
    )

    list_plots = images_plots + analysis_plots

    dir = os.path.join(
        path,
        f"{couple[0]}_{couple[1]}",
        f"{coloc_filename_1}-{coloc_filename_2}_{cyclone_name}_{acq_date}",
    )
    os.makedirs(dir, exist_ok=True)
    i = 0
    for img in images_plots:
        pn.Row(img).save(
            os.path.join(
                dir, f"{coloc_filename_1}-{coloc_filename_2}_image{str(i)}.png"
            )
        )
        i += 1

    i = 0
    for p in analysis_plots:
        pn.Row(p).save(
            os.path.join(
                dir, f"{coloc_filename_1}-{coloc_filename_2}_analysis{str(i)}.png"
            )
        )
        i += 1

    # renderLayout(list_plots, isServer=True, analysis_panel=pnWindSpeedInfos)


def all_sat_coloc_png(path):
    coloc_couples = [("S1A", "RS2"), ("S1B", "RS2"), ("C-Band_SAR", "SMAP")]

    for couple in coloc_couples:
        os.makedirs(os.path.join(path, f"{couple[0]}_{couple[1]}"), exist_ok=True)
        failed = 0
        ok = 0
        api_req = (
            f"https://cyclobs.ifremer.fr/app/api/getData"
            + f"?include_cols=all&coloc_input={quote(couple[0])}&coloc_comparison={quote(couple[1])}"
        )
        coloc_idx_ok = []
        print(api_req)
        df_coloc = pd.read_csv(api_req, keep_default_na=False)
        df_coloc["acquisition_start_time"] = pd.to_datetime(
            df_coloc["acquisition_start_time"]
        )
        df_coloc["order"] = 0
        df_coloc["order"] = df_coloc.apply(
            lambda x: 0 if x["mission_short"] == couple[0] else 1, axis=1
        )
        df_coloc = df_coloc.sort_values(by=["order"])
        for index, row in df_coloc.iterrows():
            coloc_idx = row["coloc_index"]
            if coloc_idx not in coloc_idx_ok:
                coloc_idx_ok.append(coloc_idx)
                same_cidx = df_coloc.loc[
                    (df_coloc["coloc_index"] == coloc_idx)
                    & (df_coloc["data_url"] != row["data_url"])
                ].iloc[0]
                try:
                    sat_coloc_plots_png(
                        os.path.basename(row["data_url"]),
                        os.path.basename(same_cidx["data_url"]),
                        path,
                        couple,
                        row["cyclone_name"],
                        row["acquisition_start_time"].strftime("%d-%m-%Y"),
                    )
                    ok += 1
                except Exception as e:
                    failed += 1

        logger.info(f"Ok count : {ok}. Failed count: {failed}")
