import logging

from cyclobs_utils import get_missions_per_instrument, get_all_instruments
from .sfmr_concat import concat_data
from .utils import get_track_sources

from bokeh.io import curdoc, export_png
from bokeh.layouts import layout
from holoviews import link_selections, opts
from holoviews.operation import histogram
from holoviews.operation.datashader import rasterize
import panel as pn
import numpy as np
import pandas as pd
import geopandas as gpd
import xarray as xr
import rasterio
import os
import copy
import holoviews as hv
import geoviews as gv
import geoviews.tile_sources as gts
import cyclone_formatter
import glob
import colormap_ext
from bokeh.models import Div, Legend, LegendItem, CustomJSHover, HoverTool, Row, Column
import datetime
import humanize
import pathurl
import matplotlib.cm as cm
import requests

logger = logging.getLogger(__name__)


renderer = hv.renderer('bokeh').instance()
delta_times = [7200, 1800]
dtime_colors = {7200: "red", 1800: "cyan"}
dtime_markers = {7200: '^', 1800: 'o'}


def sfmr_sat_img_plot(sat_file, sat_file_save, path, df_dt, width=300, height=300, holo=False):
    ds = cyclone_formatter.readFormatted(sat_file)
    vdim = hv.Dimension("wind_speed", range=(0, 80))
    cmap = cm.get_cmap("high_wind_speed")
    gvds = gv.Dataset(ds["wind_speed"], vdims=[vdim])
    img = gvds.to(gv.Image, ['lon', 'lat']).opts(cmap=cmap, width=width, height=height, tools=["hover", "save"], colorbar=True)

    # df, ok = sfmr_reader.read_sfmr(sfmr_file, sfmr_source)

    pts_dt = {}
    for dt, coloc_df in df_dt.items():
        gdf = gpd.GeoDataFrame(
            coloc_df, geometry=gpd.points_from_xy(coloc_df.sfmr_lon, coloc_df.sfmr_lat))
        pts = gv.Points(gdf, vdims=["sfmr_wind_speed"])
        pts.opts(cmap=cmap, fill_color="sfmr_wind_speed", size=5, line_width=1, line_color="black",
                 clim=(0, 80), tools=["box_select", "lasso_select"])
        pts_dt[dt] = pts
    p = (gts.EsriImagery() * img * hv.Overlay(list(pts_dt.values()))).opts(tools=["hover", "save"])

    if holo:
        pane = pn.Row(p)
        path = os.path.join(path, sat_file_save + "_img.png")
        pane.save(path)
        #return p, pts_dt
    else:
        hvplot = renderer.get_plot(p, curdoc())
        return hvplot.state


# Creating a SFMR wind speed curve, a SAR wind speed curve and a hv.Area of sfmr rain rate
def profile_plot(sat_file, path, df_dt, holo=False, dt_lines=7200):
    # Function to format axis ticks labels
    def delta_formatter(value):
        sec = value / 1000
        days = int(sec / 86400)
        sec_remain = sec % 86400
        hours = int(sec_remain / 3600)
        sec_remain = sec_remain % 3600
        minutes = int(sec_remain / 60)
        sec_remain = sec_remain % 60
        # return "" + str(value)
        return "" + str(days) + " day " + str(hours) + "h" + ":" + str(minutes)  # + ":" + sec_remain

    # Format "date" field of hover tooltip
    timedelta_custom = CustomJSHover(code="""
        var projections = Bokeh.require("core/util/projections");
        let sec = value / 1000;
        let days = Math.trunc(sec / 86400);
        let sec_remain = sec % 86400;
        let hours = Math.trunc(sec_remain / 3600);
        sec_remain = sec_remain % 3600;
        let minutes = Math.trunc(sec_remain / 60);
        sec_remain = sec_remain % 60;

        //return "" + days + " d. " + hours + " h. " + minutes + " m. " + sec_remain + " s.";
        //return "" + days + " " + sec;
        return "" + days + " day " + hours + ":" + minutes + ":" + sec_remain;
    """)

    # Creating hover tools
    h_sfmr = HoverTool(tooltips=[("Delta time", "@delta_time{custom}"), ("SFMR wind speed", "@sfmr_wind_speed")])
    h_sfmr.formatters = {"@delta_time": timedelta_custom}

    h_sat = HoverTool(tooltips=[("Delta time", "@delta_time{custom}"), ("Sat. wind speed", "@sat_wind_speed")])
    h_sat.formatters = {"@delta_time": timedelta_custom}

    scat_sfmr_dt = {}
    scat_sat_dt = {}
    for dt, coloc_df in df_dt.items():
        scat_sfmr = hv.Scatter(coloc_df, kdims=["delta_time"], vdims=["sfmr_wind_speed"], label="SFMR wind speed") \
            .opts(tools=["hover", "box_select", "lasso_select"], selection_color="green", color="blue",
                  nonselection_alpha=0, selection_alpha=1, size=3)
        scat_sat = hv.Scatter(coloc_df, kdims=["delta_time"], vdims=["sat_wind_speed"], label="Sat. wind speed") \
            .opts(tools=["hover", "box_select", "lasso_select"], selection_color="green", color="orange",
                  nonselection_alpha=0, selection_alpha=1, size=3)
        scat_sfmr_dt[dt] = scat_sfmr
        scat_sat_dt[dt] = scat_sat

    p = (hv.Curve(df_dt[dt_lines], kdims=["delta_time"], vdims=["sfmr_wind_speed"], label="SFMR wind speed").opts(
        line_color="blue", tools=[h_sfmr]) *
         hv.Curve(df_dt[dt_lines], kdims=["delta_time"], vdims=["sat_wind_speed"], label="Sat. wind speed").opts(
             line_color="orange", tools=[h_sat]) *
         hv.Overlay(list(scat_sfmr_dt.values()) + list(scat_sat_dt.values())) *
         hv.Area(df_dt[dt_lines], kdims=["delta_time"], vdims=["sfmr_rain_rate"], label="SFMR rain rate")
         .opts(tools=["hover", "lasso_select", "box_select"], fill_alpha=0.3, fill_color="gray", line_color="gray",
               line_alpha=0.7))

    p.opts(height=300, width=800, xformatter=delta_formatter, responsive=True, xlabel="Delta time",
           ylabel="Wind speed [m/s] / Rain rate", tools=["box_select", "lasso_select"], show_grid=True)

    if not holo:
        hvplot = renderer.get_plot(p, curdoc())

        return layout(hvplot.state, sizing_mode="stretch_width")
    else:
        pane = pn.Row(p)
        path = os.path.join(path, sat_file + "_profile.png")
        pane.save(path)
        #return p, scat_sfmr_dt, scat_sat_dt


def scatter(sat_file, path, df_dt, title, stats, link=lambda x: x, full_df=None, holo=False, width_height=300, lims=(0, 120),
            raster=False, save=False, save_path="./"):
    def stat_legend(plot, element):
        l = Legend(location="bottom_right", glyph_width=3, label_standoff=0, margin=0,
                   padding=0,
                   spacing=-1, label_height=0, label_text_line_height=0, label_text_font_size="12px")
        i = 0
        for s in stats:
            stat_str = f"{s.ljust(4)} = "
            dt_str = "△t    = "
            for dt in stats[s]:
                tm_delta = datetime.timedelta(seconds=dt)
                stat_str += f"{str(stats[s][dt]).ljust(7)}| "
                dt_str += f"{humanize.naturaldelta(tm_delta).replace('hours', 'h').replace('minutes', 'm').ljust(8)}| "

            dt_str = dt_str.rstrip("| ")
            if i == 0:
                l.items.append(LegendItem(label=dt_str))
            stat_str = stat_str.rstrip("| ")
            l.items.append(LegendItem(label=stat_str))
            i += 1

        plot.state.add_layout(l, "center")

    if full_df is not None:
        d_plots = [hv.Scatter(full_df, kdims=["sat_wind_speed"], vdims=["sfmr_wind_speed"]).opts(
            tools=["box_select", "lasso_select"],
            nonselection_alpha=0.1,
            selection_alpha=0.3,
            nonselection_fill_alpha=0.1,
            selection_fill_alpha=0.3)]
        d_hist_y = []  # histogram(d_plots[0], dimension="sfmr_wind_speed", normed=False)]
        d_hist_x = []  # histogram(d_plots[0], dimension="sat_wind_speed", normed=False)]
        scats_dt = {}
    else:
        d_plots = []
        scats_dt = {}
        d_hist_y = []
        d_hist_x = []
    for delta, df in df_dt.items():
        dt = datetime.timedelta(seconds=delta)
        nb = len(df.loc[~pd.isna(df["sat_wind_speed"])].index)
        scat = hv.Scatter(df, kdims=["sfmr_wind_speed"],
                          label=f"△t < {humanize.naturaldelta(dt).replace('hours', 'h').replace('minutes', 'm')}, {nb} points",
                          vdims=["sat_wind_speed"]).opts(
            tools=["box_select", "lasso_select", "hover"], show_grid=True, color=dtime_colors[delta], width=width_height)
        d_hist_y.append(histogram(scat, dimension="sat_wind_speed", normed=False))
        d_hist_x.append(histogram(scat, dimension="sfmr_wind_speed", normed=False))
        d_plots.append(scat)
        scats_dt[delta] = scat

    d_plots.append(hv.Curve([[0, 0], [80, 80]], "sfmr_wind_speed", "sat_wind_speed").opts(color="black"))
    hists_y = hv.Overlay(d_hist_y).opts(xticks=2, width=125, show_legend=False, xlabel="Sat. wind speed [m/s]")
    hists_x = hv.Overlay(d_hist_x).opts(yticks=5, height=150, xlabel="SFMR wind speed [m/s]")
    if width_height is None:
        hists_y.opts(responsive=True)
        hists_x.opts(responsive=True)

    ov = hv.Overlay(d_plots).opts(xlabel="SFMR wind speed [m/s]", ylabel="Sat. wind speed [m/s]",
                                  title=title, show_grid=True, tools=["box_select", "lasso_select", "save", "hover"],
                                  legend_position="top_left", xlim=lims,
                                  ylim=lims, hooks=[stat_legend])

    if width_height is not None:
        ov.opts(width=width_height, height=width_height)
    else:
        ov.opts(responsive=True)

    p = (ov << hists_y << hists_x).opts(opts.Histogram(alpha=0.3))

    if holo:
        pane = pn.Row(p)
        path = os.path.join(path, sat_file + "_scat.png")
        pane.save(path)
        #return p, scats_dt
    else:
        hvplot = renderer.get_plot(p, curdoc()).state
        if save==True:
            title = title.replace(" ","-")
            export_png(hvplot, filename=os.path.join(save_path, title+".png"))
            logger.info(title+".png generated")
            return title+".png"
        else:
            hvplot.sizing_mode = "scale_both"
            return layout(hvplot, sizing_mode="scale_both")


def nrcs_scatter(sat_file, path, name_suffix, df_dt, pol, ylim, width_height=500, holo=False, title="", histo=False, dropna=True, save=False, save_path="./"):
    scats = {}
    d_hist_y = []
    d_hist_x = []
    for dt, df in df_dt.items():
        tm_delta = datetime.timedelta(seconds=dt)
        str_d = humanize.naturaldelta(tm_delta).replace('hours', 'h').replace('minutes', 'm')
        df[f"sat_nrcs_{pol}_db"] = 10 * np.log10(df[f"sat_nrcs_{pol}"])
        if dropna:
            df = df.loc[~df[f"sat_nrcs_{pol}_db"].isin([np.nan, np.inf, -np.inf])]
        scat = hv.Scatter(df, kdims=["sfmr_wind_speed"],
                          vdims=[f"sat_nrcs_{pol}_db", "sat_incidence_angle"],
                          label=f"△t < {str_d}").opts(
            tools=["box_select", "lasso_select", "hover"], show_grid=True, color="sat_incidence_angle",
            title=title,
            xlim=(0, 80), clabel="Incid. angle",
            xlabel="SFMR wind speed [m/s]", ylabel=f"Sat. NRCS-{pol} [db]",
            color_levels=[15, 17.5, 20, 22.5, 25, 27.5, 30, 32.5, 35, 37.5, 40, 42.5, 45, 47.5, 50],
            cmap='Category20',
            colorbar=True,
            line_width=0.3,
            line_color="black",
            size=4, ylim=ylim, marker=dtime_markers[dt], legend_position='bottom_right')

        if width_height is not None:
            scat.opts(width=width_height, height=width_height)
        else:
            scat.opts(responsive=True)

        if dt == 7200:
            scat.opts(alpha=0.2, muted_fill_alpha=0.05, muted_alpha=0.05, selection_alpha=0.6)
        scats[dt] = scat
        d_hist_y.append(histogram(scat, dimension=f"sat_nrcs_{pol}_db", normed=False))
        d_hist_x.append(histogram(scat, dimension="sfmr_wind_speed", normed=False))

    hists_y = hv.Overlay(d_hist_y).opts(xticks=2, width=125, show_legend=False, xlabel=f"Sat. NRCS-{pol} [db]")
    hists_x = hv.Overlay(d_hist_x).opts(yticks=5, height=150, xlabel="SFMR wind speed [m/s]")
    if width_height is None:
        hists_y.opts(responsive=True)
        hists_x.opts(responsive=True)
    if histo:
        p = (hv.Overlay(list(scats.values())).opts(ylim=ylim) << hists_y << hists_x).opts(opts.Histogram(alpha=0.3))
    else:
        p = hv.Overlay(list(scats.values()))

    if holo:
        pane = pn.Row(p)
        path = os.path.join(path, sat_file + "_nrcs" + name_suffix + ".png")
        pane.save(path)
        #return p, scats
    else:
        hvplot = renderer.get_plot(p, curdoc()).state
        if save==True:
            title = title.replace(" ","-")
            export_png(hvplot, filename=os.path.join(save_path, title+".png"))
            logger.info(title+".png generated")
            return title+".png"
        else:
            hvplot.sizing_mode = "scale_both"
            return layout(hvplot, sizing_mode="scale_both")


def stats(df_dt):
    s = {"bias": {}, "corr": {}, "std": {}}
    ss = {}
    for dt in df_dt:
        ds = df_dt[dt]
        bias = (ds["sat_wind_speed"] - ds["sfmr_wind_speed"]).mean(skipna=True)
        if isinstance(ds, pd.DataFrame):
            corr = ds["sat_wind_speed"].corr(ds["sfmr_wind_speed"])
        elif isinstance(ds, xr.Dataset):
            corr = xr.corr(ds["sat_wind_speed"], ds["sfmr_wind_speed"])
        else:
            raise TypeError("Only xarray.Dataset and pandas.Dataframe can be used.")
        std = (ds["sat_wind_speed"] - ds["sfmr_wind_speed"]).std(skipna=True)
        s["bias"][dt] = round(float(bias), 2)
        s["corr"][dt] = round(float(corr), 2)
        s["std"][dt] = round(float(std), 2)

        ss[dt] = {"bias": round(float(bias), 2), "corr": round(float(corr), 2), "std": round(float(std), 2)}

    return s

def get_plot_data_from_file(sfmr_source, filepath, instrument, delta_time, mission=None):
    search_str = f"*_{sfmr_source}_{instrument}"
    if mission is not None:
        search_str += f"_{mission}_{delta_time}.nc"
    else:
        search_str += f"_{delta_time}.nc"

    search_str = os.path.join(filepath, search_str)
    files = glob.glob(search_str)

    if len(files) > 0:
        ds = xr.open_dataset(files[0]).to_dataframe()
        return ds
    else:
        msg = f"Concatenated SFMR coloc data file not found in : {search_str}"
        logger.warning(msg)
        return None

def create_plots(api_host, sfmr_file_path, save_path="./", commit=None, config=None, listing=None):
    logger.debug("smfr path : "+sfmr_file_path)
    logger.info("Get instruments list")
    instruments = get_all_instruments(api_host, commit=commit, config=config, listing=listing)
        
    logger.info("Get missions per instrument list")
    missions = get_missions_per_instrument(api_host, commit=commit, config=config, listing=listing)

    logger.info("Get sfmr sources")
    sfmr_sources = get_track_sources(api_host, commit=commit, config=config, listing=listing)
    sfmr_sources = [s for s in sfmr_sources if "sfmr" in s]

    sat_file =""
    path=""
    name_suffix=""

    logger.info("Generate data for page creation")
    data = {}
    data["title"] = "Overview"
    data["href"] = "Overview"

    for sfmr in sfmr_sources:
        logger.info("Generate "+sfmr+" coloc plots")
        data[sfmr] = {}
        data[sfmr]["title"] = sfmr.replace("_", " ").upper()
        data[sfmr]["href"] = data["href"] + sfmr

        for inst in instruments:
            logger.info("Generate "+sfmr+" "+inst+" coloc plots")
            data[sfmr][inst] = {}
            data[sfmr][inst]["title"] = inst
            data[sfmr][inst]["href"] = sfmr + "_" + inst

            ds_instrs = {}
            for dt in delta_times:
                ds_tmp = get_plot_data_from_file(sfmr, sfmr_file_path, instrument=inst, delta_time=dt)
                if ds_tmp is not None:
                    ds_instrs[dt] = ds_tmp
            if len(ds_instrs) > 0:
                sts = stats(ds_instrs)
                data[sfmr][inst]["chart"] = [
                    scatter(sat_file,path,ds_instrs, title=sfmr+" All missions", stats=sts, width_height=500, save=True, save_path=save_path),
                    nrcs_scatter(sat_file,path,name_suffix,ds_instrs, pol="cross", ylim=(-45, -10),
                                                                width_height=500, holo=False, histo=True,
                                                                title=sfmr+" All missions NRCS-crosspol", save=True, save_path=save_path),
                    nrcs_scatter(sat_file,path,name_suffix,ds_instrs, pol="co", ylim=(-25, 5),
                                                                width_height=500, holo=False, histo=True,
                                                                title=sfmr+" All missions NRCS-copol", save=True, save_path=save_path)
                ]
                
            else:
                data[sfmr][inst]["chart"] = [
                    "No data for building scatter plot."
                ]

            for mis in missions[inst]:
                ds_miss = {}
                for dt in delta_times:
                    ds_tmp = get_plot_data_from_file(sfmr, sfmr_file_path, instrument=inst, mission=mis, delta_time=dt)
                    if ds_tmp is not None:
                        ds_miss[dt] = ds_tmp
                logger.info("ds_miss : "+str(ds_miss))
                if len(ds_miss) > 0:
                    sts = stats(ds_miss)
                    data[sfmr][inst]["chart"].append(
                        scatter(sat_file,path,ds_miss, title=sfmr+" "+mis, stats=sts, width_height=500, save=True, save_path=save_path))
                    data[sfmr][inst]["chart"].append(
                        nrcs_scatter(sat_file,path,name_suffix, ds_miss, pol="cross", ylim=(-45, -15), histo=True,
                                    width_height=500, holo=False, title=sfmr+" "+mis + " NRCS-crosspol", save=True, save_path=save_path))
                    data[sfmr][inst]["chart"].append(
                        nrcs_scatter(sat_file,path,name_suffix, ds_miss, pol="co", ylim=(-25, 5), histo=True,
                                    width_height=500, holo=False, title=sfmr+" "+mis + " NRCS-copol", save=True, save_path=save_path))


def sfmr_coloc_png(coloc_df, sat_file, cyclone_name, mission, acq_date, source, path):
    df_dt = {}
    coloc_df = coloc_df.sort_index()
    coloc_df = coloc_df.reset_index()

    sat_file_save = os.path.basename(sat_file).replace(".nc", "") + "_" + source
    dir_name = f"{cyclone_name}_{mission}_{acq_date.strftime('%d-%m-%Y')}_{source}_{sat_file_save}"
    path = os.path.join(path, dir_name)
    os.makedirs(path, exist_ok=True)

    for dt in delta_times:
        # ds_dt[dt] = coloc_df.sel(delta_time=slice(, f"{dt} seconds"))
        df_tmp = copy.copy(coloc_df.loc[(coloc_df["delta_time"] > pd.Timedelta(-dt, unit='s')) & (
                coloc_df["delta_time"] < pd.Timedelta(dt, unit='s'))])
        if len(df_tmp.loc[~pd.isna(df_tmp["sat_wind_speed"])].index) > 0:
            df_dt[dt] = df_tmp

    if len(df_dt) > 0:
        sts = stats(df_dt)
        scatter(sat_file_save, path, df_dt, title="SFMR vs Satellite", stats=sts, full_df=None, holo=True,
                                  width_height=700, lims=(0, 80))
        nrcs_scatter(sat_file_save, path, "cross", df_dt, ylim=(-45, -15), pol="cross", width_height=700, holo=True,
                                                       title="SFMR wind speed vs Satellite NRCS-crosspol",
                                                       histo=False, dropna=False)
        nrcs_scatter(sat_file_save, path, "co", df_dt, ylim=(-25, 5), pol="co", width_height=700, holo=True,
                                                       title="SFMR wind speed vs Satellite NRCS-copol",
                                                       histo=False, dropna=False)
        profile_plot(sat_file_save, path, df_dt, holo=True)
        sfmr_sat_img_plot(sat_file, sat_file_save, path, df_dt, height=700, width=700, holo=True)


def sfmr_coloc_list(path_save):
    sfmr_sources = ["sfmr_NESDIS", "sfmr_HRD"]
    fail_count = 0

    logger.info("Starting...")

    data_sources = {}
    for source in sfmr_sources:
        # WARNING here you should NOT change this with url_for("api.acquisitionList", _external=True), because it doesnt
        # work as it is ran in another thread
        api_req = f"https://cyclobs.ifremer.fr/app/api/getData" \
                  + f"?track_source={source}&product_type=gridded&include_cols=all"

        print(api_req)
        df_coloc = pd.read_csv(api_req)

        # Only keep coloc with a generated product file
        df_coloc = df_coloc[(df_coloc["coloc_file"] != "") & (~pd.isna(df_coloc["coloc_file"]))]

        list_cols = ["sfmr_wind_speed", "sat_wind_speed", "sat_nrcs_cross", "sat_nrcs_co", "sat_incidence_angle"]
        list_col_opt = ["sat_nrcs_cross", "sat_nrcs_co", "sat_incidence_angle"]

        df_coloc["coloc_file"] = df_coloc["coloc_file"].apply(
            lambda x: pathurl.toPath(x, schema="dmz"))

        df_coloc["acquisition_start_time"] = pd.to_datetime(df_coloc["acquisition_start_time"])
        #logger.info(df_coloc["acquisition_start_time"])
        for index, row in df_coloc.iterrows():
            sat_file = pathurl.toPath(row["data_url"], schema="dmz")
            coloc_file = pathurl.toPath(row["coloc_file"], schema="dmz")
            coloc_df = xr.open_dataset(coloc_file).to_dataframe()

            try:
                sfmr_coloc_png(coloc_df, sat_file, row["cyclone_name"], row["mission_or_instr"], row["acquisition_start_time"], source, path_save)
            except Exception as e:
                logger.error(f"Failed to generate plots for file {coloc_file}")
                fail_count += 1

    logger.info(f"{fail_count} coloc plot generation has failed.")


def create_concat_plots(api_host, path, commit=None, config=None, listing=None):
    instruments = get_all_instruments(api_host, commit=commit, config=config, listing=listing)
    missions = get_missions_per_instrument(api_host, commit=commit, config=config, listing=listing)
    sfmr_sources = get_track_sources(api_host, commit=commit, config=config, listing=listing)
    sfmr_sources = [s for s in sfmr_sources if "sfmr" in s]

    ds_instr, ds_mis = concat_data(api_host, commit=commit, config=config, listing=listing)

    logger.info("Starting to create plots...")

    data = {}
    data["title"] = "Overview"
    data["href"] = "Overview"
    for sfmr in sfmr_sources:
        data[sfmr] = {}
        data[sfmr]["title"] = sfmr.replace("_", " ").upper()
        data[sfmr]["href"] = data["href"] + sfmr

        for inst in instruments:
            data[sfmr][inst] = {}
            data[sfmr][inst]["title"] = inst
            data[sfmr][inst]["href"] = sfmr + "_" + inst

            #for dt in delta_times:
            #    ds_tmp = ds_instr#get_plot_data_from_file(sfmr, instrument=inst, delta_time=dt)
            #    if ds_tmp is not None:
            #        ds_instrs[dt] = ds_tmp

            if inst in ds_instr[sfmr] and len(ds_instr[sfmr][inst]) > 0:
                ds_instrs = ds_instr[sfmr][inst]
                sts = stats(ds_instrs)
                scatter(f"{sfmr}_{inst.replace(' ', '_')}", path, ds_instrs, title="All missions", stats=sts, width_height=700, holo=True,
                        lims=(0, 80))
                #nrcs_scatter(sat_file, path, name_suffix, ds_instrs, pol="cross", ylim=(-45, -10),
                #             width_height=500, holo=False, histo=True,
                #             title=sfmr + " All missions NRCS-crosspol", save=True, save_path=save_path),
                nrcs_scatter(f"{sfmr}_{inst.replace(' ', '_')}", path, "cross", ds_instrs, ylim=(-45, -15), pol="cross",
                             width_height=700,
                             holo=True,
                             title="All missions NRCS-crosspol",
                             histo=True, dropna=False)

                nrcs_scatter(f"{sfmr}_{inst.replace(' ', '_')}", path, "co", ds_instrs, ylim=(-25, 5), pol="co", width_height=700,
                             holo=True,
                             title="All missions NRCS-copol",
                             histo=True, dropna=False)

            for mis in missions[inst]:
                if mis in ds_mis[sfmr] and len(ds_mis[sfmr][mis]) > 0:
                    ds_miss = ds_mis[sfmr][mis]
                    sts = stats(ds_miss)
                    scatter(f"{sfmr}_{mis.replace(' ', '_')}", path, ds_miss, title=mis, stats=sts, width_height=700,
                            holo=True,
                            lims=(0, 80))
                    nrcs_scatter(f"{sfmr}_{mis.replace(' ', '_')}", path, "cross", ds_miss, ylim=(-45, -15), pol="cross",
                                 width_height=700,
                                 holo=True,
                                 title=mis + " NRCS-crosspol",
                                 histo=True, dropna=False)

                    nrcs_scatter(f"{sfmr}_{mis.replace(' ', '_')}", path, "co", ds_miss, ylim=(-25, 5), pol="co",
                                 width_height=700,
                                 holo=True,
                                 title=mis + " NRCS-copol",
                                 histo=True, dropna=False)

    return data

